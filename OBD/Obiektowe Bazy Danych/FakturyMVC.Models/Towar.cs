﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Models
{
    public class Towar
    {
        public string NazwaTowaru { get; set; }
        public string PodatekVat { get; set; }
        public double CenaBazowa { get; set; }
    }
}
