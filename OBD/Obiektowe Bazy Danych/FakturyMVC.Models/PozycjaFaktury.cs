﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Models
{
    public class PozycjaFaktury
    {
        public Towar Towar { get; set; }
        public int Ilosc { get; set; }
    }
}
