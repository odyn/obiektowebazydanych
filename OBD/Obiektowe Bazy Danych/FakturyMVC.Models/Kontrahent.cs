﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Models
{
    public class Kontrahent
    {
        public string NazwaKontrahenta { get; set; }
        public string Nip { get; set; }
        public Adres Adres { get; set; }
    }
}
