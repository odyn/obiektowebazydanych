﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Models
{
    public class Faktura
    {
        public DateTime DataWystawienia { get; set; }
        public int NumerFaktury { get; set; }
        public Kontrahent Odbiorca { get; set; }
        public List<PozycjaFaktury> Towary { get; set; }
    }
}
