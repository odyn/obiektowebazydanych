﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakturyMVC.Models;
using FakturyMVC.Repository.Abstrakcja;

namespace FakturyMVC.Services.Services
{
    public class KontrahentService : IKontrahentService
    {
        public IRepozytorium<Kontrahent> KontrahentRepozytorium { get; private set; }
        public IRepozytorium<Adres> AdresRepozytorium { get; private set; }

        public KontrahentService(IRepozytorium<Kontrahent> kontrahentRepo, IRepozytorium<Adres> adresRepo  )
        {
            this.KontrahentRepozytorium = kontrahentRepo;
            this.AdresRepozytorium = adresRepo;
        }
    }
}
