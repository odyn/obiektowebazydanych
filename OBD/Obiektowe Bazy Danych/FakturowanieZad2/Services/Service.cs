﻿using System.Collections;
using System.Collections.Generic;

namespace FakturowanieZad2.Services
{
    public class Service
    {
        public static IList<string> GetTaxOptions { get; set; }
        public static IList<string> GetFilteringOptions { get; set; }
        public Service()
        {
            GetTaxOptions = new[] {"0%", "8%", "23%", "zwolniony"};
            GetFilteringOptions = new[] {"Nazwie kontrahenta", "Numerz NIP", "Ulicy", "Kodzie Pocztowym", "Mieście"};
        }
    }
}
