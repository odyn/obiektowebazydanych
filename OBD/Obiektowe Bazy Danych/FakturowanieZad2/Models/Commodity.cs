﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieZad2.Models
{
    public class Commodity
    {
        public string Amount { get; set; }
        public string CommodityName { get; set; }
        private string _nettoPrice;

        public string NettoPrice
        {
            get { return Convert.ToDouble(_nettoPrice).ToString("N2"); }
            set { _nettoPrice = value; }
        }
    }
}
