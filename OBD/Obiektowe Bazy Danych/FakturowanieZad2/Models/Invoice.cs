﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieZad2.Models
{
    public class Invoice
    {
        public string DateOfIssue { get; set; }
        public int InvoiceNumber { get; set; }
        public Contractor Reciever { get; set; }
        public List<InvoicePossition> Commoditions { get; set; }

        private string amount;
        private double amountDouble = 0;

        public string Amount
        {
            get
            {
                if (Commoditions == null)
                    return amount = "0.00";

                var kwotaDouble = Commoditions.Sum(commodity => Convert.ToDouble(commodity.PriceNetto));

                return Convert.ToDouble(amountDouble).ToString("N2");
            }
            set { amount = value; }
        }

        public List<Taxes> Totality { get; set; } // zestawienie

        private void CalculateTax()
        {
            throw new NotImplementedException();
        }

        private void CalculateAmount()
        {
            throw new NotImplementedException();
        }

        public void CreateTotality()
        {
            if (Totality != null)
                Totality.Clear();

            Totality = new List<Taxes>
            {
                new Taxes("23%", "0.00"),
                new Taxes("8%", "0.00"),
                new Taxes("0%", "0.00"),
                new Taxes("zwolniony", "0.00")
            };

            if (Commoditions == null)
                return;



            foreach (var commodity in Commoditions.Where(commodity => commodity.Commodity.Amount == "23%"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto);
                Totality.First(n => n.Tax == "23%").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }

            amountDouble = 0;
            foreach (var commodity in Commoditions.Where(commodity => commodity.Commodity.Amount == "8%"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto);
                Totality.First(n => n.Tax == "8%").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }

            amountDouble = 0;
            foreach (var commodity in Commoditions.Where(commodity => commodity.Commodity.Amount == "0%"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto);
                Totality.First(n => n.Tax == "0%").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }

            amountDouble = 0;
            foreach (var commodity in Commoditions.Where(towar => towar.Commodity.Amount == "zwolniony"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto);
                Totality.First(n => n.Tax == "zwolniony").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}
