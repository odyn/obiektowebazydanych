﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieZad2.Models
{
    public class InvoicePossition
    {
        public Commodity Commodity { get; set; }
        public int Quantity { get; set; }

        private string _priceNetto;

        public string PriceNetto
        {
            get { return Convert.ToDouble(_priceNetto).ToString("N2"); }
            set { _priceNetto = value; }
        }
    }
}
