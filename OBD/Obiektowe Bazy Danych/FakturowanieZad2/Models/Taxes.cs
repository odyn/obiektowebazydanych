﻿namespace FakturowanieZad2.Models
{
    public class Taxes
    {
        public Taxes(string tax, string price)
        {
            this.Tax = tax;
            this.Price = price;
        }
        public string Tax { get; set; }
        public string Price { get; set; }
    }
}
