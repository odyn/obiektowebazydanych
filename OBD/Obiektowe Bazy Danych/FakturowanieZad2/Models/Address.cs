﻿namespace FakturowanieZad2.Models
{
    public class Address
    {
        public string City { get; set; }
        public string Street { get; set; }
        public string PostalCode { get; set; }

        public Address(string city, string street, string code)
        {
            this.City = city;
            this.Street = street;
            this.PostalCode = code;
        }


    }
}
