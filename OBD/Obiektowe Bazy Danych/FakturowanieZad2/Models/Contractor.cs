﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieZad2.Models
{
    public class Contractor
    {
        public string ContractorName { get; set; }
        public string NipNumber { get; set; }
        public Address Address { get; set; }
    }
}
