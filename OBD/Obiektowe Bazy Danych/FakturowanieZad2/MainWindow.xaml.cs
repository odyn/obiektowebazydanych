﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ShowAllContractors();
        }

        private void MainWindow_OnActivated(object sender, EventArgs e)
        {
            ShowAllContractors();
        }

        public void ShowAllContractors()
        {
            DataGridViewMain.CommitEdit();

            var contractors = DataBaseConnection.db.Query<Contractor>().ToList();

            var contractorsToRemove = new List<Contractor>();


            foreach (var cont in contractorsToRemove)
            {
                contractors.Remove(cont);
            }

            DataGridViewMain.ItemsSource = contractors;
        }

        private void CreateKontrahentButtonClicked(object sender, RoutedEventArgs e)
        {
            new CreateContractorWindow().Show();
        }

        private void ShowAllInvoicesButton_Click(object sender, RoutedEventArgs e)
        {
            // pobiera aktualnie istniejący element z bazy
            var contractor = ((FrameworkElement)sender).DataContext as Contractor;

            new ShowInvoicesWindow(contractor).Show();
        }

        private void EditCurrentContractorRow_Click(object sender, RoutedEventArgs e)
        {
            var contractorToRemove = ((FrameworkElement)sender).DataContext as Contractor;

            new EditCurrentContractorWindow(contractorToRemove).Show();
        }

        private void RemoveContractorRow_Click(object sender, RoutedEventArgs e)
        {
            var contractorsToRemove = ((FrameworkElement)sender).DataContext as Contractor;
            var contractorInvoices = DataBaseConnection.db.Query<Invoice>().Where(n => n.Reciever.ContractorName == contractorsToRemove.ContractorName);

            foreach (var faktura in contractorInvoices)
            {
                DataBaseConnection.db.Delete(faktura);
                DataBaseConnection.db.Commit();
            }

            DataBaseConnection.db.Delete(contractorsToRemove);
            DataBaseConnection.db.Commit();

            ShowAllContractors();
        }

        private void ShowContractorsAllInvoices_Click(object sender, RoutedEventArgs e)
        {
            var contractor = ((FrameworkElement)sender).DataContext as Contractor;

            new ShowInvoicesWindow(contractor).Show();
        }
    }
}
