﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    public partial class EditCommodityWindow : Window
    {
        public Invoice invoice;
        private readonly InvoicePossition editCommodition;

        public List<TextBox> createTextBoxes = new List<TextBox>();
        public EditCommodityWindow()
        {
            InitializeComponent();
        }

        public EditCommodityWindow(Invoice inv, InvoicePossition editCommodity)
        {
            InitializeComponent();
            invoice = inv;
            editCommodition = editCommodity;

            LoadInitValues();

            createTextBoxes.Add(CommodityNameTextBox);
            createTextBoxes.Add(PriceNettoTextBox);
            createTextBoxes.Add(QuantityTextBox);
        }

        private void LoadInitValues()
        {
            Title = editCommodition.Commodity.CommodityName + "-" + editCommodition.PriceNetto.ToString(CultureInfo.InvariantCulture) + "-" +
                    editCommodition.Quantity.ToString(CultureInfo.InvariantCulture) + "-" + editCommodition.Commodity.Amount;

            CommodityNameTextBox.Text = editCommodition.Commodity.CommodityName;
            PriceNettoTextBox.Text = editCommodition.PriceNetto.ToString(CultureInfo.InvariantCulture);
            QuantityTextBox.Text = editCommodition.Quantity.ToString(CultureInfo.InvariantCulture);
            CbSearchList.SelectedValue = editCommodition.Commodity.Amount;
        }


        private void CancelEditingButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveChangesButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            foreach (var textBox in createTextBoxes.Where(textBox => textBox.Text == "")) //Czy są puste
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole nazwa jest wymagane\n";
                return;
            }

            if (CbSearchList.SelectedValue == null) //Czy CB jest pusty
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Proszę wybrać stawkę!\n";
                return;
            }

            if (Regex.IsMatch(PriceNettoTextBox.Text, @"^\$?(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole cena netto musi posiadać tylko cyfry\n";
                return;
            }

            if (Regex.IsMatch(QuantityTextBox.Text, @"\d") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole ilość musi zawierać tylko cyfry!\n";
                return;
            }

            if (PriceNettoTextBox.Text.Count() >= 3 && PriceNettoTextBox.Text.Contains(".") == false) //Automatyczna kropka
            {
                var newString = PriceNettoTextBox.Text.Insert(PriceNettoTextBox.Text.Count() - 2, ".");
                PriceNettoTextBox.Text = newString;
            }

            foreach (var position in invoice.Commoditions.Where(pozycja => pozycja == editCommodition))
            {
                position.Quantity = Convert.ToInt32(QuantityTextBox.Text);
                position.PriceNetto = PriceNettoTextBox.Text;
                position.Commodity = new Commodity{ NettoPrice = PriceNettoTextBox.Text, CommodityName = CommodityNameTextBox.Text, Amount = CbSearchList.SelectedValue.ToString() };
            }

            DataBaseConnection.db.Store(invoice);
            DataBaseConnection.db.Commit();

            LabelError.Visibility = Visibility.Visible;
            LabelError.Foreground = Brushes.Green;

            Close();
        }
    }
}
