﻿using System;
using System.Collections;
using System.Linq;
using System.Windows;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    /// <summary>
    /// Interaction logic for ShowInvoicesWindow.xaml
    /// </summary>
    public partial class ShowInvoicesWindow : Window
    {
        private readonly Contractor contractor;

        public ShowInvoicesWindow()
        {
            InitializeComponent();
        }
        public ShowInvoicesWindow(Contractor contractor)
        {
            InitializeComponent();
            if (contractor != null)
            {
                Title = "Faktury dla "+contractor.ContractorName;
                this.contractor = contractor;
            }

            ShowInvoices();
        }

        private void ShowInvoices()
        {
            DataGridViewAllInvoices.CommitEdit();

            IList contractors = new ArrayList();

            if (contractor != null)
                contractors = DataBaseConnection.db.Query<Invoice>().Where(n => n.Reciever.ContractorName == contractor.ContractorName).ToList();

            if (contractor == null)
            {
                BtnAddNewInvoice.Visibility = Visibility.Collapsed;
                contractors = DataBaseConnection.db.Query<Invoice>().ToList();
            }

            DataGridViewAllInvoices.ItemsSource = contractors;
        }

        private void ShowInvoicesWindow_OnActivated(object sender, EventArgs e)
        {
            ShowInvoices();
        }

        private void BtnAddNewInvoice_Click(object sender, RoutedEventArgs e)
        {
            var inv = new Invoice
            {
                Reciever = contractor, 
                DateOfIssue = DateTime.Now.ToShortDateString(), 
                InvoiceNumber = CheckUniqualityOfNumber()
            };

            DataBaseConnection.db.Store(inv);
            DataBaseConnection.db.Commit();

            ShowInvoices();
        }

        // sprawdzenie unikalności numeru faktury
        private static int CheckUniqualityOfNumber()
        {
            for (var i = 1; i < int.MaxValue; i++)
            {
                if (DataBaseConnection.db.Query<Invoice>().Any(n => n.InvoiceNumber == i) == false)
                {
                    return i;
                }
            }
            return 0;
        }

        private void SaveInvoiceButton_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void TotalButton_Click(object sender, RoutedEventArgs e)
        {
            var inv = ((FrameworkElement)sender).DataContext as Invoice;

            new TotalWindow(inv).Show();
        }

        private void CommoditiesButton_Click(object sender, RoutedEventArgs e)
        {
            var inv = ((FrameworkElement)sender).DataContext as Invoice;

            new CommoditiesWindow(inv).Show();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var invoiceToRemove = ((FrameworkElement)sender).DataContext as Invoice;
            DataBaseConnection.db.Delete(invoiceToRemove);
            DataBaseConnection.db.Commit();
            ShowInvoices();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            var invoiceEdition = ((FrameworkElement)sender).DataContext as Invoice;

            new EditInvoiceWindow(invoiceEdition).Show();
        }
    }
}
