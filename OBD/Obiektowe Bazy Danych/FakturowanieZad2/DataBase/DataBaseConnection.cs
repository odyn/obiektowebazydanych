﻿using System;
using Db4objects.Db4o;

namespace FakturowanieZad2.DataBase
{
    public static class DataBaseConnection
    {
        public static IObjectContainer db;
        static string path = @"E:\Faktury";
        static DataBaseConnection()
        {    
            var configuration = Db4oEmbedded.NewConfiguration();
            configuration.Common.UpdateDepth = 4;

            try
            {
                db = Db4oEmbedded.OpenFile(configuration, path);
            }
            catch (Exception msg)
            {
                throw msg;
            }
        }



    }
}
