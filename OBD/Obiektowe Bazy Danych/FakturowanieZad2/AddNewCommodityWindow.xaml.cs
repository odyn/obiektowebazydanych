﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{

    public partial class AddNewCommodityWindow : Window
    {
        public readonly Invoice invoice;
        public List<TextBox> tbxList = new List<TextBox>();
        public AddNewCommodityWindow(Invoice invoice)
        {
            InitializeComponent();
            this.invoice = invoice;

            tbxList.Add(CommoditionNameTextBox);
            tbxList.Add(NettoPriceTextBox);
            tbxList.Add(QuantityTextBox);
        }

        private void CancelAddingCommodityButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddNewComodityToInvoiceButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            // sprawdzamy czy pola sa puste
            foreach (var textBox in tbxList.Where(textBox => textBox.Text == ""))
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole nazwa jest wymagane!";
                return;
            }

            // sprawdzamy czy combox jest pusty
            if (CbSearchList.SelectedValue == null)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Proszę wybrać stawkę!";
                return;
            }

            if (Regex.IsMatch(NettoPriceTextBox.Text, @"(?<=^| )\d+(\.\d+)?(?=$| )") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole cena netto musi zawierać tylko cyfry!";
                return;
            }

            if (Regex.IsMatch(QuantityTextBox.Text, @"\d") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole ilosc musi zawierać tylko cyfry!";
                return;
            }

            // ustawianie kropki w automatyczny sposób
            if (NettoPriceTextBox.Text.Count() >= 3 && NettoPriceTextBox.Text.Contains(".") == false)
            {
                var newString = NettoPriceTextBox.Text.Insert(NettoPriceTextBox.Text.Count() - 2, ".");
                NettoPriceTextBox.Text = newString;
            }

            // jeśli faktura jest pusta 
            if (invoice.Commoditions == null)
                invoice.Commoditions = new List<InvoicePossition>();

            // dodawanie nowych towarów
            invoice.Commoditions.Add(new InvoicePossition
            {
                PriceNetto = NettoPriceTextBox.Text,
                Quantity = Convert.ToInt32(QuantityTextBox.Text),
                Commodity = new Commodity
                {
                    NettoPrice = NettoPriceTextBox.Text,
                    CommodityName = CommoditionNameTextBox.Text,
                    Amount = CbSearchList.SelectedValue.ToString()
                }
            });

            DataBaseConnection.db.Store(invoice);
            DataBaseConnection.db.Commit();

            LabelError.Visibility = Visibility.Visible;
            LabelError.Foreground = Brushes.Green;
            LabelError.Content = "Dodano Towar pomyślnie!";

            foreach (var contr in tbxList)
            {
                contr.Text = "";
            }
        }
    }
}
