﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    
    public partial class EditInvoiceWindow : Window
    {
        private readonly Invoice invoiceEdition;
        public EditInvoiceWindow(Invoice invoice)
        {
            InitializeComponent();
            this.invoiceEdition = invoice;

            GetExsisitingInvoiceData();
        }

        // wczytawanie istniejących danych do formularza faktury
        private void GetExsisitingInvoiceData()
        {
            Title = "Edycja dla "+ invoiceEdition.Reciever.ContractorName;

            var contra = GetAllContractors("");

            foreach (var con in contra)
            {
                CbContractor.Items.Add(con);
            }
        }

        // pobieranie istniejacych kontrachentow
        private List<Contractor> GetAllContractors(string lst)
        {
            var pattern = new Regex(lst, RegexOptions.IgnoreCase);

            return lst == "" ? DataBaseConnection.db.Query<Contractor>().ToList() : DataBaseConnection.db.Query<Contractor>().Where(n => pattern.IsMatch(n.ContractorName)).ToList();
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            // zmiana daty na podstawie datepickera
            if (datePicker.SelectedDate != null) 
                invoiceEdition.DateOfIssue = datePicker.SelectedDate.Value.ToShortDateString();

            // zmiana odbiorcy
            if (CbContractor.SelectedValue != null)
                invoiceEdition.Reciever = GetAllContractors("").First(n => n.ContractorName == CbContractor.SelectedValue.ToString());


            // zapis do bazy danych
            DataBaseConnection.db.Store(invoiceEdition);
            DataBaseConnection.db.Commit();

            this.Close();
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TbxContractorNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            CbContractor.Items.Clear();

            foreach (var contractor in GetAllContractors(TbxContractorNameTextBox.Text))
            {
                CbContractor.Items.Add(contractor.ContractorName);
            }
        }

        private void ContractorName_OnGotFocus(object sender, RoutedEventArgs e)
        {
            TbxContractorNameTextBox.Text = "";
        }
    }
}
