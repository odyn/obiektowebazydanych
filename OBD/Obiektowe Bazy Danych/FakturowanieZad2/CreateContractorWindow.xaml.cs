﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    /// <summary>
    /// Interaction logic for CreateContractorWindow.xaml
    /// </summary>
    public partial class CreateContractorWindow : Window
    {
        public List<TextBox> tbxList = new List<TextBox>();
        public CreateContractorWindow()
        {
            InitializeComponent();

            tbxList.Add(ContractorNameTextBox);
            tbxList.Add(NumberNipTextBox);
            tbxList.Add(StreeTextBox);
            tbxList.Add(PostalCodeTextBox);
            tbxList.Add(CityTextBox);
        }

        private void AddNewContractorButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            // sprawdzamy czy jakies pola sa puste
            foreach (var textBox in tbxList.Where(textBox => textBox.Text == ""))
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Żadne pole nie może być puste!";
                return;
            }

            //// sprawdzamy poprawność poszczególnych pól
            //if (Regex.IsMatch(ContractorNameTextBox.Text, @"\d"))
            //{
            //    LabelError.Visibility = Visibility.Visible;
            //    LabelError.Content += "Nazwa kontrahenta może zawierać tylko litery";
            //    return;
            //}
            
            if (Regex.IsMatch(NumberNipTextBox.Text, @"^\d{10}$") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "NIP musi zawierać 10 cyfr!";
                return;
            }
            
            if (Regex.IsMatch(PostalCodeTextBox.Text, @"[0-9]{2}-[0-9]{3}") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Zły format kodu pocztowego";
                return;
            }

            if (Regex.IsMatch(CityTextBox.Text, @"\d"))
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Miasto nie może zawierać liter!";
                return;
            }


            // jeśli numer NIP dodawanego kontrahenta już istnieje to zwraca błąd
            if (DataBaseConnection.db.Query<Contractor>().Any(n => n.NipNumber == NumberNipTextBox.Text))
            {
                var cont = DataBaseConnection.db.Query<Contractor>().First(n => n.NipNumber == NumberNipTextBox.Text);
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Taki NIP już istnieje!";
                return;
            }

            // tworzenie obiektu nowego kontrachenta
            var contr = new Contractor()
            {
                Address = new Address(CityTextBox.Text, StreeTextBox.Text, PostalCodeTextBox.Text),
                ContractorName = ContractorNameTextBox.Text,
                NipNumber = NumberNipTextBox.Text
            };

            // dodawanie obiektu do bazy
            DataBaseConnection.db.Store(contr);
            DataBaseConnection.db.Commit();

            LabelError.Visibility = Visibility.Visible;
            LabelError.Foreground = Brushes.Green;
            LabelError.Content = "Dodano Kontrahenta pomyślnie!";

            // czyszczenie textboxów po dodaniu
            foreach (var cbx in tbxList)
            {
                cbx.Text = "";
            }

            //zamykanie formularza
            this.Close();

           
        }

        private void CancelAddingContractorButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
