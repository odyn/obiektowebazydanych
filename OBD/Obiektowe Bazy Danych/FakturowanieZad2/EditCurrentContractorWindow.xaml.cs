﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    /// <summary>
    /// Interaction logic for EditCurrentContractorWindow.xaml
    /// </summary>
    public partial class EditCurrentContractorWindow : Window
    {
        public List<TextBox> tbxList= new List<TextBox>();
        private readonly Contractor contractorToEdit;

        public EditCurrentContractorWindow(Contractor contractor)
        {
            InitializeComponent();
            Title = "Edycja dla " + contractor.ContractorName;

            contractorToEdit = contractor;

            tbxList.Add(ContractorNameTextBox);
            tbxList.Add(NipNumberTextBox);
            tbxList.Add(StreetTextBox);
            tbxList.Add(PostalCodeTextBox);
            tbxList.Add(CityTextBox);

            LoadExsistingData();
        }

        public void LoadExsistingData()
        {
            ContractorNameTextBox.Text = contractorToEdit.ContractorName;
            NipNumberTextBox.Text = contractorToEdit.NipNumber;
            StreetTextBox.Text = contractorToEdit.Address.Street;
            PostalCodeTextBox.Text = contractorToEdit.Address.PostalCode;
            CityTextBox.Text = contractorToEdit.Address.City;
        }

        private void CancelEditionButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveChangesButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            // sprawdzania czy pola nie sa puste
            foreach (var textBox in tbxList.Where(textBox => textBox.Text == ""))
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Wszystkie pola są wymagane!";
                return;
            }

            //if (Regex.IsMatch(ContractorNameTextBox.Text, @"\d"))
            //{
            //    LabelError.Visibility = Visibility.Visible;
            //    LabelError.Content += "Pole kontrahent moze zawierać tylko litery!";
            //    return;
            //}

            if (Regex.IsMatch(NipNumberTextBox.Text, @"^\d{10}$") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Numer NIP to 10 cyfr!";
                return;
            }

            if (Regex.IsMatch(PostalCodeTextBox.Text, @"[0-9]{2}-[0-9]{3}") == false)
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole kod pocztowy musi być w \n formacie [00-000]";
                return;
            }

            if (Regex.IsMatch(CityTextBox.Text, @"\d"))
            {
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Pole miasto może zawierać tylko litery!";
                return;
            }

            // wyciaganie kontrahenta z bazy
            var any = DataBaseConnection.db.Query<Contractor>().FirstOrDefault(n => n.NipNumber == NipNumberTextBox.Text);

            // sprawdzenie czy nowo wpisany NIP nie widnieje juz w bazie
            if (any != null && any.NipNumber != contractorToEdit.NipNumber)
            {
                var kont = DataBaseConnection.db.Query<Contractor>().First(n => n.NipNumber == NipNumberTextBox.Text);
                LabelError.Visibility = Visibility.Visible;
                LabelError.Content += "Podany NIP jest już zarejestrowany!";
                return;
            }


            // przepisywanie nowych wartości do obiektu
            contractorToEdit.ContractorName = ContractorNameTextBox.Text;
            contractorToEdit.NipNumber = NipNumberTextBox.Text;
            contractorToEdit.Address.Street = StreetTextBox.Text;
            contractorToEdit.Address.PostalCode = PostalCodeTextBox.Text;
            contractorToEdit.Address.City = CityTextBox.Text;

            // zapisywanie nowego obiektu do bazy
            DataBaseConnection.db.Store(contractorToEdit);
            DataBaseConnection.db.Commit();

            this.Close();
        }
    }
}
