﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturyMVC.UI.ViewModels
{
    public class KontrahentViewModel
    {
        public string Nazwa { get; set; }
        public string Nip { get; set; }
        public string Ulica { get; set; }
        public string Miasto { get; set; }
        public string KodPocztowy { get; set; }
    }
}