﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakturyMVC.Models;
using FakturyMVC.Services.Services;

namespace FakturyMVC.UI.Controllers
{
    public class KontrahentController : Controller
    {
        private readonly IKontrahentService _kontrahentService;

        public KontrahentController(IKontrahentService kontrahentService)
        {
            this._kontrahentService = kontrahentService;
        }
        public ActionResult Add(Kontrahent kontrahent)
        {


            this._kontrahentService.KontrahentRepozytorium.DodajObiekt(kontrahent);
            return View();
        }

        public ActionResult PokarzWszystko()
        {
            var lista = this._kontrahentService.KontrahentRepozytorium.PobierzWszystko();
            return View(lista);
        }
	}
}