﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using FakturyMVC.Repository.Abstrakcja;
using FakturyMVC.Repository.BazaDanych;
using FakturyMVC.Services.Services;

namespace FakturyMVC.UI
{
    public class DependencyConfig
    {
        public static void RegisterDependencies()
        {
            var builder = new ContainerBuilder();
            builder.RegisterGeneric(typeof(Repository<>)).As(typeof(IRepozytorium<>));
            builder.RegisterType<KontrahentService>().As<IKontrahentService>();
            builder.RegisterInstance(new Instancje()).As<IBazaDanych>();
            builder.RegisterControllers(Assembly.GetAssembly(typeof(MvcApplication)));

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}