﻿namespace FakturyUoW.Models
{
    public class PozycjaFaktury
    {
        public Towar towar { get; set; } // referencja do obiektu towar
        public int Ilosc { get; set; }
        public double Cena { get; set; } // cena zaokraglona w gore do groszy
    }
}