﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Db4objects.Db4o;

namespace FakturyUoW.Models
{
    public class AdressSession : Session
    {
        internal AdressSession(IObjectServer server):base(server){}

        public IQueryable<Address> Addresses
        {
            get { return All<Address>(); }
        }

        public Address pobierzAdresPoMiescie(string id)
        {
            return Single<Address>(s => s.Miasto.ToString() == id);
        }

        public int pobierzZamowienia(string storyGuid)
        {
            return Where<Towar>(c => c.Nazwa== storyGuid).Count();
        }
    }
}