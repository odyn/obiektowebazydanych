﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db4objects.Db4o;
using System.Linq.Expressions;

namespace FakturyUoW.Models
{
    public interface ISession : IDisposable
    {
        void CommitChanges();
        IObjectContainer Container { get;}
        void Delete<T>(T item);
        void Delete<T>(Func<T,bool> expression );
        void DeleteAll<T>();
        void Dispose();
        T Single<T>(Func<T, bool> expression);
        IQueryable<T> All<T>();
        IQueryable<T> Where<T>(Func<T, bool> expression);
        void Save<T>(T item);
    }
}
