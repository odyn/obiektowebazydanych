﻿using System;
using System.Collections.Generic;

namespace FakturyUoW.Models
{
    public class Faktura
    {
        public Faktura()
        {
            Data = DateTime.UtcNow;
        }
        public DateTime Data { get; set; }
        public int Numer { get; set; }
        public Kontrahent Odbiorca { get; set; }
        public List<PozycjaFaktury> towary { get; set; } // wtf????
        public double Kwota { get; set; } // suma wszystkich kwot z towarów
        public double Podatek { get; set; }
        public List<Faktura> Zestawienie { get; set; } // zestawienie sum kwot dla poszczegolnych podatkow
        // ('podatek A', 299,75), ('Podatek B', 0), itd
        public void ObliczPodatek()
        {

        }

        //suma kwot przemnozona przez podatki. dla przkladu
        // wyzej bedzie 60 * 123% + 30 * 108% + 40 * 100% + 50 * 100%
        public void ObliczKwote()
        {

        }

        // funkcja ma pokazac List<var> zestawienie
        public void DajZestawienie()
        {

        }
    }
}