﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using Db4objects.Db4o;

namespace FakturyUoW.Models
{
    public class SessionFactory
    {
        private static ISession _current;
        private static IObjectServer _server;

        public static ISession CreateSession()
        {
            if (_server == null)
            {
                _server = Db4oFactory.OpenServer(@"E:\Faktury", 0);
            }
            return new AdressSession(_server);
        }

        public static AdressSession Current
        {
            get
            {
                if (_current == null)
                {
                    _current = CreateSession();
                }
                return (AdressSession) _current;
            }
        }

    }
}