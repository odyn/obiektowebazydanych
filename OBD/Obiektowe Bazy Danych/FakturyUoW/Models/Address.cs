﻿using System.Linq;
using Db4objects.Db4o;

namespace FakturyUoW.Models
{
    public class Address
    {
       
        // te własciowisci nie moga byc puste!
        public string Ulica { get; set; }
        public string Miasto { get; set; }
        public string KodPocztowy { get; set; }
    }
}