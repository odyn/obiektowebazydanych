﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Db4objects.Db4o;
using Db4objects.Db4o.Linq;

namespace FakturyUoW.Models
{
    public class Session : IDisposable,ISession
    {
        private IObjectContainer db;
        public IObjectContainer Container
        {
            get
            {
                return db;
            }
        }

        public Session(IObjectServer server)
        {
            db = server.OpenClient();
        }

        // zapisuje zmiany w bazie
        public void CommitChanges()
        {
            db.Commit();
        }

        // kasuje pojedynczy obiekt z bazy
        public void Delete<T>(T item)
        {
            db.Delete(item);
        }

        // kasuje obiekt z podanym warunkiem
        public void Delete<T>(Func<T, bool> expression)
        {
            var items = All<T>().Where(expression).ToList();
            items.ForEach(x => db.Delete(x));
        }

        // kasuje wszystkie obiekty
        public void DeleteAll<T>()
        {
            var items = All<T>().ToList();
            items.ForEach(x => db.Delete(x));
        }

        // zwracanie pojedynczego rekordu
        public T Single<T>(Func<T, bool> expression)
        {
            return All<T>().SingleOrDefault(expression);
        }

        // zwraca wszystkie rekordy typu T z repozytorium
        public IQueryable<T> All<T>()
        {
            return (from T items in db select items).AsQueryable();
        }

        // zwracanie rekordow z warunkiem
        public IQueryable<T> Where<T>(Func<T, bool> expression)
        {
            return All<T>().Where(expression) as IQueryable<T>;
        }

        public void Save<T>(T item)
        {
            db.Store(item);
        }

        public void Dispose()
        {
            //explicitly close
            db.Close();
            //dispose 'em
            db.Dispose();
        }
    }
}