﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakturyUoW.Models;


namespace FakturyUoW.Controllers
{
    public class AdressController : Controller
    {
        private AdressSession db = SessionFactory.Current;

        //
        // GET: /Adress/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Adress/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Adress/Create
        public ActionResult Create()
        {
            if (ModelState.IsValid)
            {
                Address a = new Address();
                a.Miasto = Request.Form["address.Miasto"];
                a.Ulica = Request.Form["address.Ulica"];
                a.KodPocztowy = Request.Form["address.KodPocztowy"];
                db.Save(a);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
            
        }

        //
        // POST: /Adress/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Adress/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Adress/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Adress/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Adress/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
