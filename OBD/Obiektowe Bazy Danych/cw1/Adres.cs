﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1
{
    public class Adres
    {
        private string _street;
        private string _postalCode;
        private string _city;

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public Adres()
        {

        }
        public Adres(string street, string postalCode, string city)
        {
            this._street = street;
            this._postalCode = postalCode;
            this._city = city;
        }


        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", _street, _postalCode, _city);
        }
    }
}
