﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1
{
    public class Telephone
    {
        private string _phoneNumber;
        private string _operator;

        public string PhoneNumber { get; set; }
        public string Operator { get; set; }
        public Telephone()
        {

        }
        public Telephone(string phoneNumber, string oper)
        {
            this._phoneNumber = phoneNumber;
            this._operator = oper;
        }


        public override string ToString()
        {
            return string.Format("{0} / {1}", _phoneNumber, _operator);
        }


    }
}
