﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Db4objects;
using Db4objects.Db4o;
using Sharpen.IO;

namespace cw1
{
    public partial class FormMain : Form
    {
        public List<Student> Studenci = new List<Student>();
        

        BindingSource bs = new BindingSource();
        public FormMain()
        {
            InitializeComponent();
            using (IObjectContainer db = Db4oFactory.OpenFile("Rejestr.yap"))
            {
                WyswietlWszystkichStudentów(db);
            }
        }

        public void btnDodajStudenta_Click(object sender, EventArgs e)
        {         
            using (IObjectContainer db = Db4oFactory.OpenFile("Rejestr.yap"))
            {
                DodajStudenta(db);
                WyswietlWszystkichStudentów(db);
            }
        }

        public void DodajStudenta(IObjectContainer db)
        {
            Student s = new Student();
            Adres a = new Adres();
            Telephone t = new Telephone();

            s.Name = tbxFirstName.Text;
            s.Surname = tbxLastName.Text;
            s.NumberOfStudent = tbxNumberOfIndex.Text;

            a.Street = tbxStreet.Text;
            a.PostalCode = tbxPostalCode.Text;
            a.City = tbxCity.Text;

            t.PhoneNumber = tbxPhoneNumber.Text;
            t.Operator = tbxOperator.Text;

            s.Adres = new List<Adres>();
            s.Adres.Add(new Adres { Street = a.Street, PostalCode = a.PostalCode, City = a.City });

            s.TelephoneNumber = (new Telephone()
            {
                PhoneNumber = t.PhoneNumber,
                Operator = t.Operator
            }).ToString(); 
              

            db.Store(s);
        }

        public void WyswietlWszystkichStudentów(IObjectContainer db)
        {
            IObjectSet result = db.Query(typeof(Student));
            ListResult(result);
        }

        public void ListResult(IObjectSet result)
        {
            tbxStudentsCount.Text = result.Count.ToString();
            foreach (Student item in result)
            {
                Studenci.Add(item);
            }
            bs.DataSource = Studenci.ToList();
            dataGridView.DataSource = bs;
        }


        private void btnSkasujStudenta_Click(object sender, EventArgs e)
        {
            using (IObjectContainer db = Db4oFactory.OpenFile("Rejestr.yap"))
            {
                SkasujKonkretnegoStudenta(db);
                WyswietlWszystkichStudentów(db);
            }
        }

        public void SkasujKonkretnegoStudenta(IObjectContainer db)
        {
            IObjectSet result = db.QueryByExample(typeof(Student));
            Student found = (Student)result.Next();
            db.Delete(found);
            foreach (DataGridViewRow item in this.dataGridView.SelectedRows)
            {
                dataGridView.Rows.RemoveAt(item.Index);
            }
        }
        
    }


}
