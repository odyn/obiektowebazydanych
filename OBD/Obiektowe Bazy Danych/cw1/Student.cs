﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cw1
{
    public class Student
    {
        private string _name;
        private string _surname;
        private string _numberofStudent;
        private List<Adres> _adres;
        private string _telephoneNumber;

        public string Name { get; set; }

        public string Surname { get; set; }

        public string NumberOfStudent { get; set; }

        public List<Adres> Adres { get; set; }

        public string TelephoneNumber { get; set; }


        public Student()
        {

        }
        public Student(string name, string surname, string numberofStudent, List<Adres> adres, string telephoneNumber)
        {
            this._name = name;
            this._surname = surname;
            this._numberofStudent = numberofStudent;
            this._adres = adres;
            this._telephoneNumber = telephoneNumber;
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}/{3}/{4}", _name, _surname, _numberofStudent, _adres, _telephoneNumber);
        }
    }
}
