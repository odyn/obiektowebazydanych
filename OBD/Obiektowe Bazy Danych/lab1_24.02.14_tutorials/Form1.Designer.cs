﻿namespace lab1_24._02._14_tutorials
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnectWithDB = new System.Windows.Forms.Button();
            this.dgDisplayScreen = new System.Windows.Forms.DataGridView();
            this.tbxCountOfItems = new System.Windows.Forms.TextBox();
            this.btnRemoveAllPilots = new System.Windows.Forms.Button();
            this.btnUpdatePilots = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgDisplayScreen)).BeginInit();
            this.SuspendLayout();
            // 
            // btnConnectWithDB
            // 
            this.btnConnectWithDB.Location = new System.Drawing.Point(669, 315);
            this.btnConnectWithDB.Name = "btnConnectWithDB";
            this.btnConnectWithDB.Size = new System.Drawing.Size(144, 23);
            this.btnConnectWithDB.TabIndex = 0;
            this.btnConnectWithDB.Text = "Add Pilots";
            this.btnConnectWithDB.UseVisualStyleBackColor = true;
            this.btnConnectWithDB.Click += new System.EventHandler(this.btnConnectWithDB_Click);
            // 
            // dgDisplayScreen
            // 
            this.dgDisplayScreen.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgDisplayScreen.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgDisplayScreen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDisplayScreen.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgDisplayScreen.Location = new System.Drawing.Point(0, 0);
            this.dgDisplayScreen.Name = "dgDisplayScreen";
            this.dgDisplayScreen.Size = new System.Drawing.Size(825, 212);
            this.dgDisplayScreen.TabIndex = 1;
            // 
            // tbxCountOfItems
            // 
            this.tbxCountOfItems.Enabled = false;
            this.tbxCountOfItems.Location = new System.Drawing.Point(13, 315);
            this.tbxCountOfItems.Name = "tbxCountOfItems";
            this.tbxCountOfItems.Size = new System.Drawing.Size(189, 20);
            this.tbxCountOfItems.TabIndex = 2;
            // 
            // btnRemoveAllPilots
            // 
            this.btnRemoveAllPilots.Location = new System.Drawing.Point(208, 312);
            this.btnRemoveAllPilots.Name = "btnRemoveAllPilots";
            this.btnRemoveAllPilots.Size = new System.Drawing.Size(144, 23);
            this.btnRemoveAllPilots.TabIndex = 3;
            this.btnRemoveAllPilots.Text = "Remove all pilots";
            this.btnRemoveAllPilots.UseVisualStyleBackColor = true;
            this.btnRemoveAllPilots.Click += new System.EventHandler(this.btnRemoveAllPilots_Click);
            // 
            // btnUpdatePilots
            // 
            this.btnUpdatePilots.Location = new System.Drawing.Point(519, 315);
            this.btnUpdatePilots.Name = "btnUpdatePilots";
            this.btnUpdatePilots.Size = new System.Drawing.Size(144, 23);
            this.btnUpdatePilots.TabIndex = 4;
            this.btnUpdatePilots.Text = "Update pilots";
            this.btnUpdatePilots.UseVisualStyleBackColor = true;
            this.btnUpdatePilots.Click += new System.EventHandler(this.btnUpdatePilots_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 350);
            this.Controls.Add(this.btnUpdatePilots);
            this.Controls.Add(this.btnRemoveAllPilots);
            this.Controls.Add(this.tbxCountOfItems);
            this.Controls.Add(this.dgDisplayScreen);
            this.Controls.Add(this.btnConnectWithDB);
            this.Name = "Form1";
            this.Text = "Tutoriale";
            ((System.ComponentModel.ISupportInitialize)(this.dgDisplayScreen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnectWithDB;
        private System.Windows.Forms.DataGridView dgDisplayScreen;
        private System.Windows.Forms.TextBox tbxCountOfItems;
        private System.Windows.Forms.Button btnRemoveAllPilots;
        private System.Windows.Forms.Button btnUpdatePilots;
    }
}

