﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Db4objects.Db4o;
using Db4objects.Db4o.Query;
using Db4objects.Db4o.Config;
using Db4objects.Db4o.Ext;

namespace lab1_24._02._14_tutorials
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public List<Pilot> listaPilotow = new List<Pilot>();
        private void btnConnectWithDB_Click(object sender, EventArgs e)
        {
                  
            using (IObjectContainer db = Db4oEmbedded.OpenFile("DB.yap"))
            {
                Pilot p1 = new Pilot("Misio Szumacher", 230);
                Pilot p2 = new Pilot("Susane Wolter", 430);
                Pilot p3 = new Pilot("Sigid Johanson", 130);
                db.Store(p1);
                db.Store(p2);
                db.Store(p3);
  

                Pilot proto = new Pilot(null, 0);
                IObjectSet result = db.QueryByExample(proto);
                ListResult(result);
                
            }
        }
        public void ListResult(IObjectSet result)
        {
            //Console.WriteLine(result.Count);
            tbxCountOfItems.Text = "Ilosc pilotow w bazie: "+ result.Count.ToString();

            foreach (Pilot item in result)
            {
                listaPilotow.Add(item);
            }
            dgDisplayScreen.DataSource = listaPilotow.ToList();
        }

        public void btnRemoveAllPilots_Click(object sender, EventArgs e)
        {
            System.IO.File.Delete("DB.yap");
        }

        private void btnUpdatePilots_Click(object sender, EventArgs e)
        {
            using (IObjectContainer db = Db4oEmbedded.OpenFile("DB.yap"))
            {
                UpdatePilot(db);
            }
        }
        public void UpdatePilot(IObjectContainer db)
        {
            IObjectSet result = db.QueryByExample(new Pilot("Misio Schumacher", 0));
            Pilot found = (Pilot)result.Next();
            found.AddPoints(30);
            db.Store(found);
            RetrieveAllPilots(db);
        }


        public void RetrieveAllPilots(IObjectContainer db)
        {
            IObjectSet result = db.QueryByExample(typeof(Pilot));
            ListResult(result);
        }


       
    }

    public class Pilot
    {
        string _name;
        int _points;

        public Pilot(string name, int points)
        {
            _name = name;
            _points = points;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public int Points
        {
            get
            {
                return _points;
            }
        }

        public void AddPoints(int points)
        {
            _points += points;
        }

        override public string ToString()
        {
            return string.Format("{0}/{1}", _name, _points);
        }
    }

}
