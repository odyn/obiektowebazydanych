﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program1
{
    public class Telefon
    {
        string _numer;
        string _operator;

        public Telefon()
        {

        }
        public Telefon(string numer, string oper)
        {
            this._numer = numer;
            this._operator = oper;
        }
        public string Numer 
        { 
            get
            {
                return _numer;
            }
            set
            {
                _numer = value;
            }
 
        }

        public string Operator
        {
            get
            {
                return _operator;
            }
            set
            {
                _operator = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", _numer, _operator);
        }

        
    }
}
