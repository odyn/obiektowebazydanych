﻿namespace Program1
{
    partial class frmAddStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDodajNowegoStudenta = new System.Windows.Forms.Button();
            this.btnAnulujDodawanie = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbxImie = new System.Windows.Forms.TextBox();
            this.tbxNazwisko = new System.Windows.Forms.TextBox();
            this.tbxNumerIndeksu = new System.Windows.Forms.TextBox();
            this.tbxAdresZamieszkania = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxTelefon = new System.Windows.Forms.TextBox();
            this.tbxOperator = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDodajNowegoStudenta
            // 
            this.btnDodajNowegoStudenta.Location = new System.Drawing.Point(12, 315);
            this.btnDodajNowegoStudenta.Name = "btnDodajNowegoStudenta";
            this.btnDodajNowegoStudenta.Size = new System.Drawing.Size(173, 23);
            this.btnDodajNowegoStudenta.TabIndex = 0;
            this.btnDodajNowegoStudenta.Text = "Dodaj studenta";
            this.btnDodajNowegoStudenta.UseVisualStyleBackColor = true;
            this.btnDodajNowegoStudenta.Click += new System.EventHandler(this.btnDodajNowegoStudenta_Click);
            // 
            // btnAnulujDodawanie
            // 
            this.btnAnulujDodawanie.Location = new System.Drawing.Point(203, 315);
            this.btnAnulujDodawanie.Name = "btnAnulujDodawanie";
            this.btnAnulujDodawanie.Size = new System.Drawing.Size(173, 23);
            this.btnAnulujDodawanie.TabIndex = 1;
            this.btnAnulujDodawanie.Text = "Anuluj";
            this.btnAnulujDodawanie.UseVisualStyleBackColor = true;
            this.btnAnulujDodawanie.Click += new System.EventHandler(this.btnAnulujDodawanie_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbxOperator);
            this.groupBox1.Controls.Add(this.tbxTelefon);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbxAdresZamieszkania);
            this.groupBox1.Controls.Add(this.tbxNumerIndeksu);
            this.groupBox1.Controls.Add(this.tbxNazwisko);
            this.groupBox1.Controls.Add(this.tbxImie);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(364, 297);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Wypełnij dane studenta";
            // 
            // tbxImie
            // 
            this.tbxImie.Location = new System.Drawing.Point(6, 41);
            this.tbxImie.Name = "tbxImie";
            this.tbxImie.Size = new System.Drawing.Size(334, 20);
            this.tbxImie.TabIndex = 0;
            // 
            // tbxNazwisko
            // 
            this.tbxNazwisko.Location = new System.Drawing.Point(6, 87);
            this.tbxNazwisko.Name = "tbxNazwisko";
            this.tbxNazwisko.Size = new System.Drawing.Size(334, 20);
            this.tbxNazwisko.TabIndex = 1;
            // 
            // tbxNumerIndeksu
            // 
            this.tbxNumerIndeksu.Location = new System.Drawing.Point(6, 131);
            this.tbxNumerIndeksu.Name = "tbxNumerIndeksu";
            this.tbxNumerIndeksu.Size = new System.Drawing.Size(334, 20);
            this.tbxNumerIndeksu.TabIndex = 2;
            // 
            // tbxAdresZamieszkania
            // 
            this.tbxAdresZamieszkania.Location = new System.Drawing.Point(6, 172);
            this.tbxAdresZamieszkania.Name = "tbxAdresZamieszkania";
            this.tbxAdresZamieszkania.Size = new System.Drawing.Size(334, 20);
            this.tbxAdresZamieszkania.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Imie";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Nazwisko";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Numer indeksu";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Adres zamieszkania";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Telefon kontaktowy";
            // 
            // tbxTelefon
            // 
            this.tbxTelefon.Location = new System.Drawing.Point(6, 214);
            this.tbxTelefon.Name = "tbxTelefon";
            this.tbxTelefon.Size = new System.Drawing.Size(334, 20);
            this.tbxTelefon.TabIndex = 10;
            // 
            // tbxOperator
            // 
            this.tbxOperator.Location = new System.Drawing.Point(6, 261);
            this.tbxOperator.Name = "tbxOperator";
            this.tbxOperator.Size = new System.Drawing.Size(334, 20);
            this.tbxOperator.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 243);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Operator";
            // 
            // frmAddStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 348);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnAnulujDodawanie);
            this.Controls.Add(this.btnDodajNowegoStudenta);
            this.Name = "frmAddStudent";
            this.Text = "Dodaj nowego studenta do bazy";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button btnDodajNowegoStudenta;
        public System.Windows.Forms.Button btnAnulujDodawanie;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbxImie;
        private System.Windows.Forms.TextBox tbxAdresZamieszkania;
        private System.Windows.Forms.TextBox tbxNumerIndeksu;
        private System.Windows.Forms.TextBox tbxNazwisko;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbxTelefon;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxOperator;
    }
}