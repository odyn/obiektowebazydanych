﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program1
{
    public class Student
    {
        string _imie;
        string _nazwisko;
        string _numer;
        string _adres;
        List<Student> _telefony;

        public Student()
        {

        }
        public Student(string imie, string nazwisko, string numer, string adres, List<Student> telefony)
        {
            this._imie = imie;
            this._nazwisko = nazwisko;
            this._numer = numer;
            this._adres = adres;
            this._telefony = telefony;
        }

        public string Imie
        {
            get
            {
                return _imie;
            }
            set
            {
                _imie = value;
            }
        }

        public string Nazwisko
        {
            get
            {
                return _nazwisko;
            }
            set
            {
                _nazwisko = value;
            }
        }

        public string Numer
        {
            get
            {
                return _numer;
            }
            set
            {
                _numer = value;
            }
        }

        public string Adres
        {
            get
            {
                return _adres;
            }
            set
            {
                _adres = value;
            }
        }

        public List<Student> Telefony
        {
            get
            {
                return _telefony;
            }
            set
            {
                _telefony = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}/{3}/{4}", _imie, _nazwisko, _numer, _adres, _telefony);
        }
    }
}
