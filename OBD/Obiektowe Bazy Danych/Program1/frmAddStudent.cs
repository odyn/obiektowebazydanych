﻿using Db4objects.Db4o;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Db4objects;

namespace Program1
{
    public partial class frmAddStudent : Form
    {
        
        public frmAddStudent()
        {
            InitializeComponent();
        }

        private void btnAnulujDodawanie_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDodajNowegoStudenta_Click(object sender, EventArgs e)
        {
            try
            {
                using (IObjectContainer db = Db4oEmbedded.OpenFile("BazaStudentow.yap"))
                {
                    Student nowyStudent = new Student();
                    nowyStudent.Imie = tbxImie.ToString();
                    nowyStudent.Nazwisko = tbxNazwisko.ToString();
                    nowyStudent.Numer = tbxNumerIndeksu.ToString();
                    nowyStudent.Adres = tbxAdresZamieszkania.ToString();

                    Telefon nowyTelefon = new Telefon();
                    nowyTelefon.Numer = tbxTelefon.ToString();
                    nowyTelefon.Operator = tbxOperator.ToString();

                    db.Store(nowyTelefon);
                    db.Store(nowyStudent);
                    IObjectSet result = db.QueryByExample(nowyStudent);
                    this.Close();
                }
            }
            catch { }
          
        }

    }
}
