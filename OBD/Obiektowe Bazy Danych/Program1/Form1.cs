﻿using Db4objects.Db4o;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Program1
{
    public partial class frmMain : Form
    {
        public List<Student> ListaStudentów = new List<Student>();    
        public frmMain()
        {
            InitializeComponent();
            Operations();
        }

        public void Operations()
        {
            if (btnAddStudent.Pressed == true)
            {
                Form f1 = new frmAddStudent();
                f1.Show();

            }
            else if (btnEditStudent.Pressed == true)
            {

            }
            else if (btnRemoveStudent.Pressed == true)
            {
               
            }
           
        }

        public void ListResult(IObjectSet result)
        {
            tbxStudentsCount.Text = "Ilosc studentów w bazie: " + result.Count.ToString();
            foreach (Student item in result)
            {
                ListaStudentów.Add(item);
            }
            dgDisplay.DataSource = ListaStudentów.ToList();
        }

        private void wyjdźToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnAddStudent_Click(object sender, EventArgs e)
        {
            Operations();
        }

        private void btnEditStudent_Click(object sender, EventArgs e)
        {

        }

        private void btnRemoveStudent_Click(object sender, EventArgs e)
        {

        }
    }
}
