﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Repository.Abstrakcja
{
    public interface IRepozytorium<TByt> : IOperacjeNaBazieDanych<TByt> where TByt : class
    {
        IEnumerable<TByt> PobierzWszystko();
        TByt WyszukajPo(TByt schemaEntity);
        IEnumerable<TByt> FiltrujPo(Func<TByt, bool> predicate);
    }
}
