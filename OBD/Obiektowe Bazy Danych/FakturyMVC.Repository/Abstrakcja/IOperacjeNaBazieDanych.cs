﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Repository.Abstrakcja
{
    public interface IOperacjeNaBazieDanych<TByt> where TByt : class
    {
        void DodajObiekt(TByt byt);
        void AktualizujObiekt(TByt byt);
    }
}
