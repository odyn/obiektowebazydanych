﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturyMVC.Repository.Abstrakcja
{
    public interface IBazaDanych
    {
        object PobierzInstancjeBazyDanych();
    }
}
