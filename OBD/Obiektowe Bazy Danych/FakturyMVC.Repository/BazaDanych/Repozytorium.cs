﻿using System;
using System.Collections.Generic;
using System.Linq;
using Db4objects.Db4o;
using FakturyMVC.Repository.Abstrakcja;

namespace FakturyMVC.Repository.BazaDanych
{
    public class Repozytorium<TByt> : IRepozytorium<TByt> where TByt : class
    {
        private readonly IObjectContainer _db;
        public Repozytorium(IBazaDanych baza)
        {
            this._db = (IObjectContainer) baza.PobierzInstancjeBazyDanych();
        }
        public void DodajObiekt(TByt byt)
        {
            this._db.Store(byt);
        }

        public void AktualizujObiekt(TByt byt)
        {
            this._db.Store(byt);
        }

        public IEnumerable<TByt> PobierzWszystko()
        {
            return this._db.Query<TByt>();
        }

        public TByt WyszukajPo(TByt schemaEntity)
        {
            return (TByt) this._db.QueryByExample(schemaEntity).Next();
        }

        public IEnumerable<TByt> FiltrujPo(Func<TByt, bool> predicate)
        {
            return this._db.Query<TByt>().Where(predicate);
        }
    }
}
