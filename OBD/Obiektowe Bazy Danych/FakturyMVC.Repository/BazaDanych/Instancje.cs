﻿using Db4objects.Db4o;
using FakturyMVC.Repository.Abstrakcja;

namespace FakturyMVC.Repository.BazaDanych
{
    public class Instancje : IBazaDanych
    {
        private readonly IObjectContainer _db;
        public Instancje() // łączenie z baza danych
        {
            this._db = Db4oEmbedded.OpenFile(@"E:\Faktury");
        }
        public object PobierzInstancjeBazyDanych()
        {
            return _db; 
        }

        ~Instancje() // zamykanie polaczenia
        {
            this._db.Close();
        }
    }
}
