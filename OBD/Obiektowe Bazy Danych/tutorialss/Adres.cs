﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Program1
{
    public class Adres
    {
        string _ulica;
        string _kodPocztowy;
        string _miasto;

        public Adres()
        {

        }
        public Adres(string ulica, string kodPocztowy, string miasto)
        {
            this._ulica = ulica;
            this._kodPocztowy = kodPocztowy;
            this._miasto = miasto;
        }

        public string Ulica
        {
            get
            {
                return _ulica;
            }
            set
            {
                _ulica = value;
            }
        }

        public string KodPocztowy
        {
            get
            {
                return _kodPocztowy;
            }
            set
            {
                _kodPocztowy = value;
            }
        }

        public string Miasto
        {
            get
            {
                return _miasto;
            }
            set
            {
                _miasto = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", _ulica, _kodPocztowy, _miasto);
        }
    }
}
