﻿namespace FakturyWF
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.kontrahentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowyKontrahentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujKontrahentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usunKontrahentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.towarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajTowarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujTowarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usunTowarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fakturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wystawFaktureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujFaktureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usunFaktureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wyjscieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridViewMain = new System.Windows.Forms.DataGridView();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.kontrahentToolStripMenuItem,
            this.towarToolStripMenuItem,
            this.fakturaToolStripMenuItem,
            this.wyjscieToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // kontrahentToolStripMenuItem
            // 
            this.kontrahentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nowyKontrahentToolStripMenuItem,
            this.edytujKontrahentaToolStripMenuItem,
            this.usunKontrahentaToolStripMenuItem});
            this.kontrahentToolStripMenuItem.Name = "kontrahentToolStripMenuItem";
            this.kontrahentToolStripMenuItem.Size = new System.Drawing.Size(78, 20);
            this.kontrahentToolStripMenuItem.Text = "Kontrahent";
            // 
            // nowyKontrahentToolStripMenuItem
            // 
            this.nowyKontrahentToolStripMenuItem.Name = "nowyKontrahentToolStripMenuItem";
            this.nowyKontrahentToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.nowyKontrahentToolStripMenuItem.Text = "Nowy kontrahent";
            this.nowyKontrahentToolStripMenuItem.Click += new System.EventHandler(this.nowyKontrahentToolStripMenuItem_Click);
            // 
            // edytujKontrahentaToolStripMenuItem
            // 
            this.edytujKontrahentaToolStripMenuItem.Name = "edytujKontrahentaToolStripMenuItem";
            this.edytujKontrahentaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.edytujKontrahentaToolStripMenuItem.Text = "Edytuj kontrahenta";
            // 
            // usunKontrahentaToolStripMenuItem
            // 
            this.usunKontrahentaToolStripMenuItem.Name = "usunKontrahentaToolStripMenuItem";
            this.usunKontrahentaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.usunKontrahentaToolStripMenuItem.Text = "Usun kontrahenta";
            // 
            // towarToolStripMenuItem
            // 
            this.towarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajTowarToolStripMenuItem,
            this.edytujTowarToolStripMenuItem,
            this.usunTowarToolStripMenuItem});
            this.towarToolStripMenuItem.Name = "towarToolStripMenuItem";
            this.towarToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.towarToolStripMenuItem.Text = "Towar";
            // 
            // dodajTowarToolStripMenuItem
            // 
            this.dodajTowarToolStripMenuItem.Name = "dodajTowarToolStripMenuItem";
            this.dodajTowarToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.dodajTowarToolStripMenuItem.Text = "Dodaj towar";
            // 
            // edytujTowarToolStripMenuItem
            // 
            this.edytujTowarToolStripMenuItem.Name = "edytujTowarToolStripMenuItem";
            this.edytujTowarToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.edytujTowarToolStripMenuItem.Text = "Edytuj towar";
            // 
            // usunTowarToolStripMenuItem
            // 
            this.usunTowarToolStripMenuItem.Name = "usunTowarToolStripMenuItem";
            this.usunTowarToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.usunTowarToolStripMenuItem.Text = "Usun towar";
            // 
            // fakturaToolStripMenuItem
            // 
            this.fakturaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wystawFaktureToolStripMenuItem,
            this.edytujFaktureToolStripMenuItem,
            this.usunFaktureToolStripMenuItem});
            this.fakturaToolStripMenuItem.Name = "fakturaToolStripMenuItem";
            this.fakturaToolStripMenuItem.Size = new System.Drawing.Size(58, 20);
            this.fakturaToolStripMenuItem.Text = "Faktura";
            // 
            // wystawFaktureToolStripMenuItem
            // 
            this.wystawFaktureToolStripMenuItem.Name = "wystawFaktureToolStripMenuItem";
            this.wystawFaktureToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.wystawFaktureToolStripMenuItem.Text = "Wystaw fakture";
            // 
            // edytujFaktureToolStripMenuItem
            // 
            this.edytujFaktureToolStripMenuItem.Name = "edytujFaktureToolStripMenuItem";
            this.edytujFaktureToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.edytujFaktureToolStripMenuItem.Text = "Edytuj fakture";
            // 
            // usunFaktureToolStripMenuItem
            // 
            this.usunFaktureToolStripMenuItem.Name = "usunFaktureToolStripMenuItem";
            this.usunFaktureToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.usunFaktureToolStripMenuItem.Text = "Usun fakture";
            // 
            // wyjscieToolStripMenuItem
            // 
            this.wyjscieToolStripMenuItem.Name = "wyjscieToolStripMenuItem";
            this.wyjscieToolStripMenuItem.Size = new System.Drawing.Size(59, 20);
            this.wyjscieToolStripMenuItem.Text = "Wyjscie";
            this.wyjscieToolStripMenuItem.Click += new System.EventHandler(this.wyjscieToolStripMenuItem_Click);
            // 
            // dataGridViewMain
            // 
            this.dataGridViewMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewMain.Location = new System.Drawing.Point(0, 24);
            this.dataGridViewMain.Name = "dataGridViewMain";
            this.dataGridViewMain.Size = new System.Drawing.Size(1184, 709);
            this.dataGridViewMain.TabIndex = 1;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1184, 733);
            this.Controls.Add(this.dataGridViewMain);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fakturowanie WF";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem kontrahentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nowyKontrahentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujKontrahentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usunKontrahentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem towarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajTowarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujTowarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usunTowarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fakturaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wystawFaktureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujFaktureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usunFaktureToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wyjscieToolStripMenuItem;
        public System.Windows.Forms.DataGridView dataGridViewMain;
    }
}

