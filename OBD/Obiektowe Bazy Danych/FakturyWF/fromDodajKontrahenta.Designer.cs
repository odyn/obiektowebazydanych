﻿namespace FakturyWF
{
    partial class fromDodajKontrahenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxNazwaKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxNumerNip = new System.Windows.Forms.TextBox();
            this.tbxUlica = new System.Windows.Forms.TextBox();
            this.tbxKodPocztowy = new System.Windows.Forms.TextBox();
            this.tbxMiasto = new System.Windows.Forms.TextBox();
            this.btnAddKontrahent = new System.Windows.Forms.Button();
            this.btnAnulujDodawanieKontrahenta = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbxNazwaKontrahenta
            // 
            this.tbxNazwaKontrahenta.Location = new System.Drawing.Point(12, 37);
            this.tbxNazwaKontrahenta.Name = "tbxNazwaKontrahenta";
            this.tbxNazwaKontrahenta.Size = new System.Drawing.Size(486, 20);
            this.tbxNazwaKontrahenta.TabIndex = 0;
            // 
            // tbxNumerNip
            // 
            this.tbxNumerNip.Location = new System.Drawing.Point(12, 83);
            this.tbxNumerNip.Name = "tbxNumerNip";
            this.tbxNumerNip.Size = new System.Drawing.Size(486, 20);
            this.tbxNumerNip.TabIndex = 1;
            // 
            // tbxUlica
            // 
            this.tbxUlica.Location = new System.Drawing.Point(12, 133);
            this.tbxUlica.Name = "tbxUlica";
            this.tbxUlica.Size = new System.Drawing.Size(242, 20);
            this.tbxUlica.TabIndex = 2;
            // 
            // tbxKodPocztowy
            // 
            this.tbxKodPocztowy.Location = new System.Drawing.Point(285, 133);
            this.tbxKodPocztowy.Name = "tbxKodPocztowy";
            this.tbxKodPocztowy.Size = new System.Drawing.Size(213, 20);
            this.tbxKodPocztowy.TabIndex = 3;
            // 
            // tbxMiasto
            // 
            this.tbxMiasto.Location = new System.Drawing.Point(12, 181);
            this.tbxMiasto.Name = "tbxMiasto";
            this.tbxMiasto.Size = new System.Drawing.Size(242, 20);
            this.tbxMiasto.TabIndex = 4;
            // 
            // btnAddKontrahent
            // 
            this.btnAddKontrahent.Location = new System.Drawing.Point(12, 224);
            this.btnAddKontrahent.Name = "btnAddKontrahent";
            this.btnAddKontrahent.Size = new System.Drawing.Size(132, 23);
            this.btnAddKontrahent.TabIndex = 5;
            this.btnAddKontrahent.Text = "Dodaj kontrahenta";
            this.btnAddKontrahent.UseVisualStyleBackColor = true;
            this.btnAddKontrahent.Click += new System.EventHandler(this.btnAddKontrahent_Click);
            // 
            // btnAnulujDodawanieKontrahenta
            // 
            this.btnAnulujDodawanieKontrahenta.Location = new System.Drawing.Point(366, 224);
            this.btnAnulujDodawanieKontrahenta.Name = "btnAnulujDodawanieKontrahenta";
            this.btnAnulujDodawanieKontrahenta.Size = new System.Drawing.Size(132, 23);
            this.btnAnulujDodawanieKontrahenta.TabIndex = 6;
            this.btnAnulujDodawanieKontrahenta.Text = "Anuluj";
            this.btnAnulujDodawanieKontrahenta.UseVisualStyleBackColor = true;
            this.btnAnulujDodawanieKontrahenta.Click += new System.EventHandler(this.btnAnulujDodawanieKontrahenta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nazwa firmy";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Numer NIP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Ulica";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(282, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Kod pocztowy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Miasto";
            // 
            // fromDodajKontrahenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 259);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnAnulujDodawanieKontrahenta);
            this.Controls.Add(this.btnAddKontrahent);
            this.Controls.Add(this.tbxMiasto);
            this.Controls.Add(this.tbxKodPocztowy);
            this.Controls.Add(this.tbxUlica);
            this.Controls.Add(this.tbxNumerNip);
            this.Controls.Add(this.tbxNazwaKontrahenta);
            this.Name = "fromDodajKontrahenta";
            this.Text = "Dodaj kontrahenta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxNazwaKontrahenta;
        private System.Windows.Forms.TextBox tbxNumerNip;
        private System.Windows.Forms.TextBox tbxUlica;
        private System.Windows.Forms.TextBox tbxKodPocztowy;
        private System.Windows.Forms.TextBox tbxMiasto;
        private System.Windows.Forms.Button btnAddKontrahent;
        private System.Windows.Forms.Button btnAnulujDodawanieKontrahenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}