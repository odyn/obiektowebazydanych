﻿using System.Windows.Forms;
using Db4objects.Db4o;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db4objects.Db4o.Config;
using System.Data;
using FakturyWF;
using FakturyWF.Klasy;

namespace Pierwszy
{
    class WarstwaDanych
    {
        private IObjectContainer db;
        private IObjectSet result;
        private IEmbeddedConfiguration configuracja = Db4oEmbedded.NewConfiguration();

        public void PolaczenieZBazaDanych()
        {
            configuracja.Common.ObjectClass(typeof(Adres)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Adres)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Kontrahent)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Kontrahent)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Faktura)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Faktura)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(PozycjaFaktury)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(PozycjaFaktury)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Towar)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Towar)).CascadeOnUpdate(true);
            db = Db4oEmbedded.OpenFile(configuracja, "Baza");
        }

        public void ZamknijPolaczenie()
        {
            db.Close();
        }

        public int CzyIstnieje(object obiektDoSprawdzenia)
        {
            return db.QueryByExample(obiektDoSprawdzenia).Count;
        }

        public void DodajNowyObiekt(object obiektDoDodania)
        {
            db.Store(obiektDoDodania);
        }

        public IObjectSet WyszukajObiekt(object obiektDoWyszukania)
        {
            result = db.QueryByExample(obiektDoWyszukania);
            return result;
        }

        public void SkasujIstniejacyObiekt(object obiektDoSkasowania)
        {
            db.Delete(obiektDoSkasowania);
        }

        public void SkasujWszystkieObiekty()
        {
            var q1 = db.QueryByExample(typeof(Towar));
            var q2 = db.QueryByExample(typeof(Kontrahent));
            var q3 = db.QueryByExample(typeof(Adres));
            var q4 = db.QueryByExample(typeof(Faktura));
            var q5 = db.QueryByExample(typeof(PozycjaFaktury));
            foreach (var item in q1)
            {
                db.Delete(item);
            }
            foreach (var item in q2)
            {
                db.Delete(item);
            }
            foreach (var item in q3)
            {
                db.Delete(item);
            }
            foreach (var item in q4)
            {
                db.Delete(item);
            }
            foreach (var item in q5)
            {
                db.Delete(item);
            }
        }

        

    }
}
