﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FakturyWF.Klasy;
using Pierwszy;

namespace FakturyWF
{
    public partial class fromDodajKontrahenta : Form
    {
        readonly WarstwaDanych _db = new WarstwaDanych();
        public fromDodajKontrahenta()
        {
            InitializeComponent();
            //_db.PolaczenieZBazaDanych();
        }

        private void btnAnulujDodawanieKontrahenta_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void btnAddKontrahent_Click(object sender, EventArgs e)
        {
            Kontrahent k = new Kontrahent();
            k.NazwaKontrahenta = tbxNazwaKontrahenta.Text;
            k.Nip = tbxNumerNip.Text;
            k.Adres = new Adres()
            {
                KodPocztowy = tbxKodPocztowy.Text,
                Ulica = tbxUlica.Text,
                Miasto = tbxMiasto.Text
            };

            if (_db.CzyIstnieje(k) == 0)
            {
                _db.DodajNowyObiekt(k);
            }
            else
            {
                MessageBox.Show("Taki obiekt juz istnieje!");
            }
            
        }
    }
}
