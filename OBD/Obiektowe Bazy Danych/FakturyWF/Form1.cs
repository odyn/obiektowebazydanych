﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Db4objects.Db4o;
using FakturyWF.Klasy;
using Pierwszy;

namespace FakturyWF
{
    public partial class FrmMain : Form
    {
        private readonly WarstwaDanych _db = new WarstwaDanych();
        public List<Kontrahent> ListaKontrahents = new List<Kontrahent>(); 
        public FrmMain()
        {
            InitializeComponent();
            _db.PolaczenieZBazaDanych();
        }

        private void wyjscieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        private void nowyKontrahentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f = new fromDodajKontrahenta();
            f.Show();
        }
    }
}
