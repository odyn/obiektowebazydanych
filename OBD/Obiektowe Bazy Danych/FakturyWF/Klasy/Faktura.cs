﻿using System;
using System.Collections.Generic;

namespace FakturyWF.Klasy
{
    public class Faktura
    {
        public DateTime DataWystawienia { get; set; }
        public int NumerFaktury { get; set; }
        public Kontrahent Odbiorca { get; set; }
        public List<PozycjaFaktury> Towary { get; set; }
    }
}
