﻿namespace FakturyWF.Klasy
{
    public class Towar
    {
        public string NazwaTowaru { get; set; }
        public string PodatekVat { get; set; }
        public double CenaBazowa { get; set; }
    }
}
