﻿namespace FakturyWF.Klasy
{
    public class Adres
    {
        public string Ulica { get; set; }
        public string Miasto { get; set; }
        public string KodPocztowy { get; set; }
    }
}
