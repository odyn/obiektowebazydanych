﻿namespace FakturyWF.Klasy
{
    public class Kontrahent
    {
        public string NazwaKontrahenta { get; set; }
        public string Nip { get; set; }
        public Adres Adres { get; set; }
    }
}
