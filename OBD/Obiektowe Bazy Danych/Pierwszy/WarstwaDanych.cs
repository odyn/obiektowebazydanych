﻿using Db4objects.Db4o;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db4objects.Db4o.Config;
using System.Data;

namespace Pierwszy
{
    class WarstwaDanych
    {
        private IObjectContainer db;
        private IObjectSet result;
        readonly IEmbeddedConfiguration configuracja = Db4oEmbedded.NewConfiguration();

        public void PolaczenieZBazaDanych()
        {
            configuracja.Common.ObjectClass(typeof(Student)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Student)).CascadeOnUpdate(true);
            db = Db4oEmbedded.OpenFile(configuracja, "BazaStudentow.yap");
        }

        public void ZamknijPolaczenie()
        {
            db.Close();
        }

        public int CzyIstnieje(object obiektDoSprawdzenia)
        {
            return db.QueryByExample(obiektDoSprawdzenia).Count;
        }

        public void DodajNowyObiekt(object obiektDoDodania)
        {
            db.Store(obiektDoDodania);
        }

        public IObjectSet WyszukajObiekt(object obiektDoWyszukania)
        {
            result = db.QueryByExample(obiektDoWyszukania);
            return result;
        }

        public void SkasujIstniejacyObiekt(object obiektDoSkasowania)
        {
            db.Delete(obiektDoSkasowania);
        }

        public void SkasujWszystkieObiekty()
        {
            var q = db.QueryByExample(typeof(Student));
            foreach (var item in q)
            {
                db.Delete(item);
            }
        }

    }
}
