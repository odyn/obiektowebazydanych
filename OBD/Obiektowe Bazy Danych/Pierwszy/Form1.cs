﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Pierwszy
{
    public partial class Form1 : Form
    {
        readonly WarstwaDanych _db = new WarstwaDanych();
        public Form1()
        {
            InitializeComponent();
            _db.PolaczenieZBazaDanych();
            WyswietlWszystkichStudentow();
        }

        private void btnDodajStudenta_Click(object sender, EventArgs e)
        {
            if (tbxImie.Text == "" || tbxNazwisko.Text == "" || tbxNumerIndeksu.Text == "" || tbxUlica.Text == "" || tbxNumerMieszkania.Text == "" || tbxKodPocztowy.Text == "" || tbxMiasto.Text == "")
            {
                MessageBox.Show("Żadne pole nie może być puste! Wypełnij wszystko!");
            }
            else
            {
                var nowystudent = new Student()
                {
                    Imie = tbxImie.Text,
                    Nazwisko = tbxNazwisko.Text,
                    NumerIndeksu = Convert.ToInt32(tbxNumerIndeksu.Text)
                };

                var nowyadres = new Adres()
                {
                    Ulica = tbxUlica.Text,
                    NumerMieszkania = Convert.ToInt32(tbxNumerMieszkania.Text),
                    KodPocztowy = tbxKodPocztowy.Text,
                    Miasto = tbxMiasto.Text
                };

                if (tbxNumerTelefonu.Text == "" && tbxOperator.Text == "")
                {
                    tbxNumerTelefonu.Text = "";
                    tbxOperator.Text = "";
                }
                else
                {
                    var nowytelefon = new Telefon()
                    {
                        NumerTelefonu = Convert.ToInt32(tbxNumerTelefonu.Text),
                        NazwaOperatora = tbxOperator.Text
                    };

                    nowystudent.DodajNowyTelefon(nowytelefon);
                }
                

                nowystudent.DodajAdresZamieszkania(nowyadres);
                

                var q = new Student { NumerIndeksu = Convert.ToInt32(tbxNumerIndeksu.Text) };

                if (_db.CzyIstnieje(q) == 0)
                {
                    _db.DodajNowyObiekt(nowystudent);
                    MessageBox.Show("Dodano studenta pomyslnie");
                    tbxWyswietl.Clear();
                    WyswietlWszystkichStudentow();
                }
                else
                {
                    MessageBox.Show("Podany numer indeksu juz widnieje w bazie!");
                }
            }
        }

        private void btnWyswietlWszystkich_Click(object sender, EventArgs e)
        {
            WyswietlWszystkichStudentow();
        }

        public void WyswietlWszystkichStudentow()
        {
            tbxWyswietl.Clear();
            var s = new Student();
            var all = _db.WyszukajObiekt(s);
            tbxCountOfStudents.Text = "Aktualnie w bazie znajduje się: " + all.Count.ToString() + " student/ów";

            foreach (var st in all.Cast<Student>())
            {
                tbxWyswietl.Text += string.Format("Student: {0} {1} Indeks: {2} {3}", st.Imie, st.Nazwisko, st.NumerIndeksu, Environment.NewLine);
                foreach (var adr in st.AdresZamieszkania)
                {
                    tbxWyswietl.Text += string.Format("Ulica: {0} Nr.domu: {1} Kod pocztowy: {2} Miasto: {3} {4}", adr.Ulica, adr.NumerMieszkania, adr.KodPocztowy, adr.Miasto, Environment.NewLine);
                    foreach (var tel in st.NumeryTelefonow)
                    {
                        tbxWyswietl.Text += string.Format("Telefony: {0} Operator: {1} {2}", tel.NumerTelefonu, tel.NazwaOperatora, Environment.NewLine);
                    }
                }
                tbxWyswietl.Text += "----------------------------------------------------------------------------------------------";
                tbxWyswietl.Text += Environment.NewLine;
            }
        }

        private void btnWyczyscWyjscie_Click(object sender, EventArgs e)
        {
            tbxWyswietl.Clear();
        }

        private void btnSkasujWybranegoStudenta_Click(object sender, EventArgs e)
        {
            if (tbxNumerIndeksu.Text == "")
            {
                MessageBox.Show("Pole Numer Indeksu nie moze byc puste!");
            }
            else
            {
                var index = new Student { NumerIndeksu = Convert.ToInt32(tbxNumerIndeksu.Text) };
                var znalezionystudent = (Student)_db.WyszukajObiekt(index).Next();

                _db.SkasujIstniejacyObiekt(znalezionystudent);
                MessageBox.Show("Student o podanym indeksie został usunięty");
            }
            
        }

        private void btnWyczyscPolaForumlarza_Click(object sender, EventArgs e)
        {
            tbxImie.Clear();
            tbxNazwisko.Clear();
            tbxNumerIndeksu.Clear();
            tbxUlica.Clear();
            tbxNumerMieszkania.Clear();
            tbxKodPocztowy.Clear();
            tbxMiasto.Clear();
            tbxNumerTelefonu.Clear();
            tbxOperator.Clear();
        }

        private void btnDodajNumerDodatkowyWgNumeruIndeksu_Click(object sender, EventArgs e)
        {
            var tel = new Telefon { NumerTelefonu = Convert.ToInt32(tbxNumerTelefonu.Text), NazwaOperatora = tbxOperator.Text };
            var index = new Student { NumerIndeksu = Convert.ToInt32(tbxNumerIndeksu.Text) };
            var student = (Student)_db.WyszukajObiekt(index).Next();
            student.DodajNowyTelefon(tel);
            _db.DodajNowyObiekt(student);

            MessageBox.Show("Dodano telefon dodatkowy");
            WyswietlWszystkichStudentow();
        }

        private void btnSkasujWybranyNumer_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Czy na pewno chcesz skasować ten numer?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (tbxNumerTelefonu.Text == "")
            {
                MessageBox.Show("Pole Numer Telefonu nie moze byc puste!");
            }
            else if (tbxNumerIndeksu.Text == "")
            {
                MessageBox.Show("Pole Numer Indexu nie moze byc puste!");
            }
            else
            {
                var index = new Student { NumerIndeksu = Convert.ToInt32(tbxNumerIndeksu.Text) };
                var tel = new Telefon { NumerTelefonu = Convert.ToInt32(tbxNumerTelefonu.Text) };
                var student = (Student)_db.WyszukajObiekt(index).Next();
                var tel2 = (Telefon)_db.WyszukajObiekt(tel).Next();
                var znaleziony = student.NumeryTelefonow.IndexOf(tel2);
                student.SkasujWybranyTelefon(znaleziony);
                _db.DodajNowyObiekt(student);
                _db.SkasujIstniejacyObiekt(tel2);

                MessageBox.Show("Usunieto numer wybranego studenta");
                WyswietlWszystkichStudentow();
            }
        }

        private void btnEdytujStudenta_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Czy na pewno chcesz dodać nowy adres?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (tbxNumerIndeksu.Text == "")
            {
                MessageBox.Show("Pole Numer Indeksu nie moze byc puste!");
            }
            else
            {
                var p = new Student { NumerIndeksu = Convert.ToInt32(tbxNumerIndeksu.Text) };

                var a = new Adres
                {
                    Ulica = tbxUlica.Text,
                    NumerMieszkania = Convert.ToInt32(tbxNumerMieszkania.Text),
                    KodPocztowy = tbxKodPocztowy.Text,
                    Miasto = tbxMiasto.Text
                };

                var s = (Student)_db.WyszukajObiekt(p).Next();
                var ca = s.AdresZamieszkania;
                _db.SkasujIstniejacyObiekt(ca);
                s.DodajAdresZamieszkania(a);
                _db.DodajNowyObiekt(s);
                MessageBox.Show("Zaktualizowano adres wybranego studenta");
                WyswietlWszystkichStudentow();
            }
            
        }

        private void btnSkasujWszystkichStudentów_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Czy jesteś pewien?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            try
            {
                _db.SkasujWszystkieObiekty();
                tbxWyswietl.Clear();
                //File.Delete("BazaStudentow.yap");
                MessageBox.Show("Baza została wyczyszczona pomyślnie!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            _db.ZamknijPolaczenie();
        }
    }
}
