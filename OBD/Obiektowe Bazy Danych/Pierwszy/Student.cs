﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pierwszy
{
    public class Student
    {
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int NumerIndeksu { get; set; }

        public List<Adres> AdresZamieszkania = new List<Adres>();
        public List<Telefon> NumeryTelefonow = new List<Telefon>();

        public void DodajNowyTelefon(Telefon nowy)
        {
            NumeryTelefonow.Add(nowy);
        }

        public void SkasujWybranyTelefon(int currentIndex)
        {
            NumeryTelefonow.RemoveAt(currentIndex);
        }

        public void DodajAdresZamieszkania(Adres nowy)
        {
            AdresZamieszkania.Add(nowy);
        }

        public void SkasujWybranyAdresZamieszkania(int currentIndex)
        {
            AdresZamieszkania.RemoveAt(currentIndex);
        }
    }
}
