﻿namespace Pierwszy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDodajStudenta = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxOperator = new System.Windows.Forms.TextBox();
            this.tbxNumerTelefonu = new System.Windows.Forms.TextBox();
            this.tbxMiasto = new System.Windows.Forms.TextBox();
            this.tbxKodPocztowy = new System.Windows.Forms.TextBox();
            this.tbxNumerMieszkania = new System.Windows.Forms.TextBox();
            this.tbxUlica = new System.Windows.Forms.TextBox();
            this.tbxNumerIndeksu = new System.Windows.Forms.TextBox();
            this.tbxNazwisko = new System.Windows.Forms.TextBox();
            this.tbxImie = new System.Windows.Forms.TextBox();
            this.tbxCountOfStudents = new System.Windows.Forms.TextBox();
            this.btnWyswietlWszystkich = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tbxWyswietl = new System.Windows.Forms.TextBox();
            this.btnWyczyscWyjscie = new System.Windows.Forms.Button();
            this.btnSkasujWybranegoStudenta = new System.Windows.Forms.Button();
            this.btnWyczyscPolaForumlarza = new System.Windows.Forms.Button();
            this.btnDodajNumerDodatkowyWgNumeruIndeksu = new System.Windows.Forms.Button();
            this.btnSkasujWybranyNumer = new System.Windows.Forms.Button();
            this.btnEdytujStudenta = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSkasujWszystkichStudentów = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnDodajStudenta);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tbxOperator);
            this.groupBox1.Controls.Add(this.tbxNumerTelefonu);
            this.groupBox1.Controls.Add(this.tbxMiasto);
            this.groupBox1.Controls.Add(this.tbxKodPocztowy);
            this.groupBox1.Controls.Add(this.tbxNumerMieszkania);
            this.groupBox1.Controls.Add(this.tbxUlica);
            this.groupBox1.Controls.Add(this.tbxNumerIndeksu);
            this.groupBox1.Controls.Add(this.tbxNazwisko);
            this.groupBox1.Controls.Add(this.tbxImie);
            this.groupBox1.Location = new System.Drawing.Point(13, 246);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(268, 336);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dane nowego studenta";
            // 
            // btnDodajStudenta
            // 
            this.btnDodajStudenta.Location = new System.Drawing.Point(-1, 313);
            this.btnDodajStudenta.Name = "btnDodajStudenta";
            this.btnDodajStudenta.Size = new System.Drawing.Size(269, 23);
            this.btnDodajStudenta.TabIndex = 2;
            this.btnDodajStudenta.Text = "Dodaj studenta do bazy";
            this.btnDodajStudenta.UseVisualStyleBackColor = true;
            this.btnDodajStudenta.Click += new System.EventHandler(this.btnDodajStudenta_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 271);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Operator";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Numer Telefonu";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(118, 193);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Miasto";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Kod pocztowy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(150, 152);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(93, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Numer mieszkania";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ulica";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Numer indeksu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Nazwisko";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Imie";
            // 
            // tbxOperator
            // 
            this.tbxOperator.Location = new System.Drawing.Point(6, 287);
            this.tbxOperator.Name = "tbxOperator";
            this.tbxOperator.Size = new System.Drawing.Size(193, 20);
            this.tbxOperator.TabIndex = 8;
            // 
            // tbxNumerTelefonu
            // 
            this.tbxNumerTelefonu.Location = new System.Drawing.Point(6, 247);
            this.tbxNumerTelefonu.Name = "tbxNumerTelefonu";
            this.tbxNumerTelefonu.Size = new System.Drawing.Size(193, 20);
            this.tbxNumerTelefonu.TabIndex = 7;
            // 
            // tbxMiasto
            // 
            this.tbxMiasto.Location = new System.Drawing.Point(121, 209);
            this.tbxMiasto.Name = "tbxMiasto";
            this.tbxMiasto.Size = new System.Drawing.Size(129, 20);
            this.tbxMiasto.TabIndex = 6;
            // 
            // tbxKodPocztowy
            // 
            this.tbxKodPocztowy.Location = new System.Drawing.Point(6, 209);
            this.tbxKodPocztowy.Name = "tbxKodPocztowy";
            this.tbxKodPocztowy.Size = new System.Drawing.Size(96, 20);
            this.tbxKodPocztowy.TabIndex = 5;
            // 
            // tbxNumerMieszkania
            // 
            this.tbxNumerMieszkania.Location = new System.Drawing.Point(153, 168);
            this.tbxNumerMieszkania.Name = "tbxNumerMieszkania";
            this.tbxNumerMieszkania.Size = new System.Drawing.Size(97, 20);
            this.tbxNumerMieszkania.TabIndex = 4;
            // 
            // tbxUlica
            // 
            this.tbxUlica.Location = new System.Drawing.Point(6, 168);
            this.tbxUlica.Name = "tbxUlica";
            this.tbxUlica.Size = new System.Drawing.Size(116, 20);
            this.tbxUlica.TabIndex = 3;
            // 
            // tbxNumerIndeksu
            // 
            this.tbxNumerIndeksu.Location = new System.Drawing.Point(6, 125);
            this.tbxNumerIndeksu.Name = "tbxNumerIndeksu";
            this.tbxNumerIndeksu.Size = new System.Drawing.Size(193, 20);
            this.tbxNumerIndeksu.TabIndex = 2;
            // 
            // tbxNazwisko
            // 
            this.tbxNazwisko.Location = new System.Drawing.Point(6, 81);
            this.tbxNazwisko.Name = "tbxNazwisko";
            this.tbxNazwisko.Size = new System.Drawing.Size(193, 20);
            this.tbxNazwisko.TabIndex = 1;
            // 
            // tbxImie
            // 
            this.tbxImie.Location = new System.Drawing.Point(6, 39);
            this.tbxImie.Name = "tbxImie";
            this.tbxImie.Size = new System.Drawing.Size(193, 20);
            this.tbxImie.TabIndex = 0;
            // 
            // tbxCountOfStudents
            // 
            this.tbxCountOfStudents.Enabled = false;
            this.tbxCountOfStudents.Location = new System.Drawing.Point(601, 246);
            this.tbxCountOfStudents.Name = "tbxCountOfStudents";
            this.tbxCountOfStudents.Size = new System.Drawing.Size(221, 20);
            this.tbxCountOfStudents.TabIndex = 2;
            // 
            // btnWyswietlWszystkich
            // 
            this.btnWyswietlWszystkich.Location = new System.Drawing.Point(17, 21);
            this.btnWyswietlWszystkich.Name = "btnWyswietlWszystkich";
            this.btnWyswietlWszystkich.Size = new System.Drawing.Size(221, 23);
            this.btnWyswietlWszystkich.TabIndex = 10;
            this.btnWyswietlWszystkich.Text = "Wyświetl wszystkich studentów";
            this.btnWyswietlWszystkich.UseVisualStyleBackColor = true;
            this.btnWyswietlWszystkich.Click += new System.EventHandler(this.btnWyswietlWszystkich_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(505, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Liczba studentów";
            // 
            // tbxWyswietl
            // 
            this.tbxWyswietl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbxWyswietl.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbxWyswietl.Location = new System.Drawing.Point(0, 0);
            this.tbxWyswietl.Multiline = true;
            this.tbxWyswietl.Name = "tbxWyswietl";
            this.tbxWyswietl.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbxWyswietl.Size = new System.Drawing.Size(834, 241);
            this.tbxWyswietl.TabIndex = 11;
            // 
            // btnWyczyscWyjscie
            // 
            this.btnWyczyscWyjscie.Location = new System.Drawing.Point(17, 60);
            this.btnWyczyscWyjscie.Name = "btnWyczyscWyjscie";
            this.btnWyczyscWyjscie.Size = new System.Drawing.Size(221, 23);
            this.btnWyczyscWyjscie.TabIndex = 12;
            this.btnWyczyscWyjscie.Text = "Wyczyść wyświetlacz";
            this.btnWyczyscWyjscie.UseVisualStyleBackColor = true;
            this.btnWyczyscWyjscie.Click += new System.EventHandler(this.btnWyczyscWyjscie_Click);
            // 
            // btnSkasujWybranegoStudenta
            // 
            this.btnSkasujWybranegoStudenta.Location = new System.Drawing.Point(17, 98);
            this.btnSkasujWybranegoStudenta.Name = "btnSkasujWybranegoStudenta";
            this.btnSkasujWybranegoStudenta.Size = new System.Drawing.Size(221, 23);
            this.btnSkasujWybranegoStudenta.TabIndex = 13;
            this.btnSkasujWybranegoStudenta.Text = "Skasuj studenta";
            this.btnSkasujWybranegoStudenta.UseVisualStyleBackColor = true;
            this.btnSkasujWybranegoStudenta.Click += new System.EventHandler(this.btnSkasujWybranegoStudenta_Click);
            // 
            // btnWyczyscPolaForumlarza
            // 
            this.btnWyczyscPolaForumlarza.Location = new System.Drawing.Point(6, 20);
            this.btnWyczyscPolaForumlarza.Name = "btnWyczyscPolaForumlarza";
            this.btnWyczyscPolaForumlarza.Size = new System.Drawing.Size(188, 23);
            this.btnWyczyscPolaForumlarza.TabIndex = 10;
            this.btnWyczyscPolaForumlarza.Text = "Wyczyść pola";
            this.btnWyczyscPolaForumlarza.UseVisualStyleBackColor = true;
            this.btnWyczyscPolaForumlarza.Click += new System.EventHandler(this.btnWyczyscPolaForumlarza_Click);
            // 
            // btnDodajNumerDodatkowyWgNumeruIndeksu
            // 
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.Location = new System.Drawing.Point(17, 137);
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.Name = "btnDodajNumerDodatkowyWgNumeruIndeksu";
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.Size = new System.Drawing.Size(221, 23);
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.TabIndex = 14;
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.Text = "Dodaj numer dodatkowy";
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.UseVisualStyleBackColor = true;
            this.btnDodajNumerDodatkowyWgNumeruIndeksu.Click += new System.EventHandler(this.btnDodajNumerDodatkowyWgNumeruIndeksu_Click);
            // 
            // btnSkasujWybranyNumer
            // 
            this.btnSkasujWybranyNumer.Location = new System.Drawing.Point(17, 170);
            this.btnSkasujWybranyNumer.Name = "btnSkasujWybranyNumer";
            this.btnSkasujWybranyNumer.Size = new System.Drawing.Size(221, 23);
            this.btnSkasujWybranyNumer.TabIndex = 15;
            this.btnSkasujWybranyNumer.Text = "Skasuj wybrany numer";
            this.btnSkasujWybranyNumer.UseVisualStyleBackColor = true;
            this.btnSkasujWybranyNumer.Click += new System.EventHandler(this.btnSkasujWybranyNumer_Click);
            // 
            // btnEdytujStudenta
            // 
            this.btnEdytujStudenta.Location = new System.Drawing.Point(17, 205);
            this.btnEdytujStudenta.Name = "btnEdytujStudenta";
            this.btnEdytujStudenta.Size = new System.Drawing.Size(221, 23);
            this.btnEdytujStudenta.TabIndex = 16;
            this.btnEdytujStudenta.Text = "Dodaj adres dodatkowy";
            this.btnEdytujStudenta.UseVisualStyleBackColor = true;
            this.btnEdytujStudenta.Click += new System.EventHandler(this.btnEdytujStudenta_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSkasujWszystkichStudentów);
            this.groupBox2.Controls.Add(this.btnWyswietlWszystkich);
            this.groupBox2.Controls.Add(this.btnEdytujStudenta);
            this.groupBox2.Controls.Add(this.btnWyczyscWyjscie);
            this.groupBox2.Controls.Add(this.btnSkasujWybranyNumer);
            this.groupBox2.Controls.Add(this.btnSkasujWybranegoStudenta);
            this.groupBox2.Controls.Add(this.btnDodajNumerDodatkowyWgNumeruIndeksu);
            this.groupBox2.Location = new System.Drawing.Point(568, 285);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(254, 278);
            this.groupBox2.TabIndex = 17;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Operacje na bazie";
            // 
            // btnSkasujWszystkichStudentów
            // 
            this.btnSkasujWszystkichStudentów.Location = new System.Drawing.Point(17, 240);
            this.btnSkasujWszystkichStudentów.Name = "btnSkasujWszystkichStudentów";
            this.btnSkasujWszystkichStudentów.Size = new System.Drawing.Size(221, 23);
            this.btnSkasujWszystkichStudentów.TabIndex = 10;
            this.btnSkasujWszystkichStudentów.Text = "Skasuj wszystkich studentów";
            this.btnSkasujWszystkichStudentów.UseVisualStyleBackColor = true;
            this.btnSkasujWszystkichStudentów.Click += new System.EventHandler(this.btnSkasujWszystkichStudentów_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnWyczyscPolaForumlarza);
            this.groupBox3.Location = new System.Drawing.Point(287, 249);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 56);
            this.groupBox3.TabIndex = 18;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Operacje na formularzu";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 594);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tbxWyswietl);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.tbxCountOfStudents);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Program1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDodajStudenta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxOperator;
        private System.Windows.Forms.TextBox tbxNumerTelefonu;
        private System.Windows.Forms.TextBox tbxMiasto;
        private System.Windows.Forms.TextBox tbxKodPocztowy;
        private System.Windows.Forms.TextBox tbxNumerMieszkania;
        private System.Windows.Forms.TextBox tbxUlica;
        private System.Windows.Forms.TextBox tbxNumerIndeksu;
        private System.Windows.Forms.TextBox tbxNazwisko;
        private System.Windows.Forms.TextBox tbxImie;
        private System.Windows.Forms.TextBox tbxCountOfStudents;
        private System.Windows.Forms.Button btnWyswietlWszystkich;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tbxWyswietl;
        private System.Windows.Forms.Button btnWyczyscWyjscie;
        private System.Windows.Forms.Button btnSkasujWybranegoStudenta;
        private System.Windows.Forms.Button btnWyczyscPolaForumlarza;
        private System.Windows.Forms.Button btnDodajNumerDodatkowyWgNumeruIndeksu;
        private System.Windows.Forms.Button btnSkasujWybranyNumer;
        private System.Windows.Forms.Button btnEdytujStudenta;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnSkasujWszystkichStudentów;
    }
}

