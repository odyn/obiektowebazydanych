﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FakturowanieWPF.DataBase;

namespace FakturowanieWPF
{
    public partial class MainWindow : Window
    {
        // klasa zawierajaca operacje na bazie danych
        readonly DataLayer _db = new DataLayer();
        public MainWindow()
        {
            InitializeComponent();
            _db.PolaczenieZBazaDanych(); // polaczenie z baza danych
            
        }

        // zamykanie programu
        private void _menuitemWyjscie_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(1);
        }


    }
}
