﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db4objects.Db4o;
using Db4objects.Db4o.Config;
using FakturowanieWPF.Models;

namespace FakturowanieWPF.DataBase
{
    public class DataLayer
    {
        private IObjectContainer _db;
        private IObjectSet _result;
        readonly IEmbeddedConfiguration configuracja = Db4oEmbedded.NewConfiguration();
        private string path = "E:\\Faktury";

        public void PolaczenieZBazaDanych()
        {
            configuracja.Common.ObjectClass(typeof(Towar)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Towar)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Faktura)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Faktura)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Adres)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Adres)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Kontrahent)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Kontrahent)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(PozycjaFaktury)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(PozycjaFaktury)).CascadeOnUpdate(true);

            _db = Db4oEmbedded.OpenFile(configuracja, path);
        }

        public void ZamknijPolaczenie()
        {
            _db.Close();
        }

        public int CzyIstnieje(object obiektDoSprawdzenia)
        {
            return _db.QueryByExample(obiektDoSprawdzenia).Count;
        }

        public void DodajNowyObiekt(object obiektDoDodania)
        {
            _db.Store(obiektDoDodania);
        }

        public IObjectSet WyszukajObiekt(object obiektDoWyszukania)
        {
            _result = _db.QueryByExample(obiektDoWyszukania);
            return _result;
        }

        public void SkasujIstniejacyObiekt(object obiektDoSkasowania)
        {
            _db.Delete(obiektDoSkasowania);
        }

        public void SkasujWszystkieObiekty()
        {
            var q = _db.QueryByExample(typeof(object));
            foreach (var item in q)
            {
                _db.Delete(item);
            }
        }

        //MainWindow m = new MainWindow();
        //public void WyswietlWszystkieObiekty()
        //{
        //    var towars = _db.Query(typeof (Towar));
        //    var kontrahents = _db.Query(typeof(Kontrahent));
        //    var adreses = _db.Query(typeof (Adres));
        //    var fakturies = _db.Query(typeof (Faktura));

        //    m.DgView.ItemsSource = towars.ToString();
        //}
    }
}
