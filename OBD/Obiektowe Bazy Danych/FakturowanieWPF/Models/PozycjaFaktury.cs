﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieWPF.Models
{
    public class PozycjaFaktury
    {
        private Towar _towar;
        private int _ilosc;
        private double _cena;

        public Towar Towar
        {
            get { return _towar; }
            set { _towar = value; }
        }

        public int Ilosc
        {
            get { return _ilosc; }
            set { _ilosc = value; }
        }

        public double Cena
        {
            get { return _cena; }
            set { _cena = value; }
        }
    }
}
