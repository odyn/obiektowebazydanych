﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieWPF.Models
{
    public class Kontrahent
    {
        private string _nazwa;
        private long _nip;
        private Adres _adres;

        public string Nazwa
        {
            get { return _nazwa; }
            set { _nazwa = value; }
        }

        public long NIP
        {
            get { return _nip; }
            set { _nip = value; }
        }

        public Adres Adres
        {
            get { return _adres; }
            set { _adres = value; }
        }

    }
}
