﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieWPF.Models
{
    public enum VAT
    {
        Dwadziesciatrzy = 23, Osiem = 8, Zero = 0, Zwolniony = 0
    }
    public class Towar
    {
        private string _nazwaTowaru;
        private VAT _stawkaVAT;

        public string NazwaTowaru
        {
            get { return _nazwaTowaru; }
            set { _nazwaTowaru = value; }
        }

        public VAT StawkaVat
        {
            get { return _stawkaVAT; }
            set { _stawkaVAT = value;  }
        }
    }
}
