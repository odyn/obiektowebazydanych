﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieWPF.Models
{
    public class Adres
    {
        private string _ulica;
        private string _miasto;
        private string _kodPocztowy;
        // te pola nie moga byc puste
        public string Ulica
        {
            get { return _ulica; }
            set { _ulica = value; }
        }
        public string Miasto 
        {
            get { return _miasto; }
            set { _miasto = value; } }

        public string KodPocztowy
        {
            get { return _kodPocztowy; }
            set { _kodPocztowy = value; }
        }
    }
}
