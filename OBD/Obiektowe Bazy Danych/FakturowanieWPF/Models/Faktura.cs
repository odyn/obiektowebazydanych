﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieWPF.Models
{
    public class Faktura
    {
        private DateTime _dataWystawienia;
        private long _numerFaktury;
        private Kontrahent _odbiorca;
        private List<Towar> _towary;
        private double _podatek;
        private List<VarEnum> _zestawienie;

        public DateTime DataWystawienia
        {
            get { return _dataWystawienia; }
            set { _dataWystawienia = value; }
        }

        public long NumerFaktury
        {
            get { return _numerFaktury; }
            set { _numerFaktury = value;  }
        }

        public Kontrahent Odbiorca
        {
            get { return _odbiorca; }
            set { _odbiorca = value; }
        }

        public List<Towar> Towary
        {
            get { return _towary; }
            set { _towary = value; }
        }

        public double Podatek
        {
            get { return _podatek; }
            set { _podatek = value; }
        }

        public List<VarEnum> Zestawienie
        {
            get { return _zestawienie; }
            set { _zestawienie = value; }
        }
    }
}
