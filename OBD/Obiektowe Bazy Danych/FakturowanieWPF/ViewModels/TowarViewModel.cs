﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakturowanieWPF.Models;

namespace FakturowanieWPF.ViewModels
{
    public class TowarViewModel : INotifyPropertyChanged
    {
        private Towar _towar;

        public Towar Towar
        {
            get { return _towar; }
            set { _towar = value; }
        }

        public string NazwaTowaru
        {
            get { return Towar.NazwaTowaru; }
            set { Towar.NazwaTowaru = value; }
        }

        public VAT StawkaVat
        {
            get { return Towar.StawkaVat; }
            set { Towar.StawkaVat = value; }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
