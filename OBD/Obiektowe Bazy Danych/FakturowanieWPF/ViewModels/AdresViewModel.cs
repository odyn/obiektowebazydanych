﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakturowanieWPF.Models;

namespace FakturowanieWPF.ViewModels
{
    public class AdresViewModel : INotifyPropertyChanged
    {
        private Adres _adres;

        public Adres Adres
        {
            get { return _adres; }
            set { _adres = value; }
        }

        public string Ulica
        {
            get { return Adres.Ulica; }
            set { Adres.Ulica = value; }
        }

        public string Miasto
        {
            get { return Adres.Miasto; }
            set { Adres.Miasto = value; }
        }

        public string KodPocztowy
        {
            get { return Adres.KodPocztowy; }
            set { Adres.KodPocztowy = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
