﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FakturowanieWPF.Models;

namespace FakturowanieWPF.ViewModels
{
    public class PozycjaFakturyViewModel : INotifyPropertyChanged
    {
        private PozycjaFaktury _pozycjaFaktury;

        public PozycjaFaktury PozycjaFaktury
        {
            get { return _pozycjaFaktury; }
            set { _pozycjaFaktury = value; }
        }

        public Towar Towar
        {
            get { return PozycjaFaktury.Towar; }
            set { PozycjaFaktury.Towar = value; }
        }

        public int Ilosc
        {
            get { return PozycjaFaktury.Ilosc; }
            set { PozycjaFaktury.Ilosc = value; }
        }

        public double Cena
        {
            get { return PozycjaFaktury.Cena; }
            set { PozycjaFaktury.Cena = value; }
        }




        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
