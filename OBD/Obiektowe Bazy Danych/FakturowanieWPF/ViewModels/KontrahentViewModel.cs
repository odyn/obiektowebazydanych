﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Animation;
using FakturowanieWPF.Models;

namespace FakturowanieWPF.ViewModels
{
    public class KontrahentViewModel : INotifyPropertyChanged
    {
        private Kontrahent _kontrahent;

        public Kontrahent Kontrahent
        {
            get { return _kontrahent; }
            set { _kontrahent = value; }
        }

        public string Nazwa
        {
            get { return Kontrahent.Nazwa; }
            set { Kontrahent.Nazwa = value; }
        }

        public long NIP
        {
            get { return Kontrahent.NIP; }
            set { Kontrahent.NIP = value; }
        }

        public Adres Adres
        {
            get { return Kontrahent.Adres; }
            set { Kontrahent.Adres = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
