﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using FakturowanieWPF.Models;

namespace FakturowanieWPF.ViewModels
{
    public class FakturaViewModel : INotifyPropertyChanged
    {
        private Faktura _faktura;

        public Faktura Faktura
        {
            get { return _faktura; }
            set { _faktura = value; }
        }

        public DateTime DataWystawienia
        {
            get { return Faktura.DataWystawienia; }
            set { Faktura.DataWystawienia = value; }
        }

        public long NumerFaktury
        {
            get { return Faktura.NumerFaktury; }
            set { Faktura.NumerFaktury = value; }
        }

        public Kontrahent Odbiorca
        {
            get { return Faktura.Odbiorca; }
            set { Faktura.Odbiorca = value; }
        }

        public List<Towar> Towary
        {
            get { return Faktura.Towary; }
            set { Faktura.Towary = value; }
        }

        public double Podatek
        {
            get { return Faktura.Podatek; }
            set { Faktura.Podatek = value; }
        }

        public List<VarEnum> Zestawienie
        {
            get { return Faktura.Zestawienie; }
            set { Faktura.Zestawienie = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
