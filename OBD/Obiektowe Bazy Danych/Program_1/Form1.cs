﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Db4objects;
using Db4objects.Db4o;

namespace Program_1
{
    
    public partial class Form1 : Form
    {
        public IObjectContainer db = Db4oEmbedded.OpenFile("BazaStudentow.yap");

        public List<Student> StudentList = new List<Student>();
        public List<Adres> AdresList = new List<Adres>();
        public List<Telephone> TelephonesList = new List<Telephone>();
       
        public Form1()
        {
            InitializeComponent();
            WyswietlWszystko(db);
        }

        public void btnDodajStudenta_Click(object sender, EventArgs e)
        { 
            try
            {
                Student std = new Student();
                Adres adr = new Adres();
                Telephone tel = new Telephone();

                std.Name = tbxName.Text;
                std.Surname = tbxSurname.Text;
                std.NumberOfStudent = tbxNumberOfIndex.Text;
                adr.Street = tbxStreet.Text;
                adr.PostalCode = tbxPostalCode.Text;
                adr.City = tbxCity.Text;
                //tel.PhoneNumber = Convert.ToInt32(tbxPhoneNumber.Text);
                //tel.Operator = tbxOperator.Text;

                std.Adres = new List<Adres>();
                std.Adres.Add(new Adres { Street = tbxStreet.Text, PostalCode = tbxPostalCode.Text, City = tbxCity.Text });
                
                std.TelephoneNumber = Convert.ToInt32(new Telephone() 
                { 
                    PhoneNumber = Convert.ToInt32(tbxPhoneNumber.Text), 
                    Operator = tbxOperator.Text 
                });
               
                IObjectSet result = db.QueryByExample(std);

                ListResult(result);
                
            }
            catch (Exception)
            {
                if (tbxName.Text == "" || tbxSurname.Text == "" || tbxNumberOfIndex.Text == "" || tbxStreet.Text == "" || tbxPostalCode.Text == "" || tbxCity.Text == "" || tbxPhoneNumber.Text == "" || tbxOperator.Text== "")
                {
                    MessageBox.Show("Prosze wypełnic wszystkie pola");
                }
                    
            }
            finally
            {
                db.Close();
            }
        }

        public void ListResult(IObjectSet result)
        {
            tbxCountOfStudentsInDatabase.Text = "Aktualnie "+result.Count.ToString()+" studentów";
            foreach (Student item in result)
            {
                StudentList.Add(item);
            }
            DGV.DataSource = StudentList.ToList();
        }

        public void WyswietlWszystko(IObjectContainer db)
        {
            try
            {
                IObjectSet result = db.Query(typeof(Student));
                ListResult(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

       
    }

    public class Student
    {
        private string _name;
        private string _surname;
        private string _numberofStudent;
        private List<Adres> _adres;
        private long _telephoneNumber;

        public string Name { get; set; }

        public string Surname { get; set; }

        public string NumberOfStudent { get; set; }

        public List<Adres> Adres { get; set; }

        public long TelephoneNumber { get; set; }


        public Student()
        {

        }
        public Student(string name, string surname, string numberofStudent, List<Adres> adres, long telephoneNumber)
        {
            this._name = name;
            this._surname = surname;
            this._numberofStudent = numberofStudent;
            this._adres = adres;
            this._telephoneNumber = telephoneNumber;
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}/{3}/{4}", _name, _surname, _numberofStudent, _adres, _telephoneNumber);
        }
    }
    public class Adres
    {
        private string _street;
        private string _postalCode;
        private string _city;

        public string Street { get; set; }

        public string PostalCode { get; set; }

        public string City { get; set; }

        public Adres()
        {

        }
        public Adres(string street, string postalCode, string city)
        {
            this._street = street;
            this._postalCode = postalCode;
            this._city = city;
        }


        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", _street, _postalCode, _city);
        }
    }

    public class Telephone
    {
        private int _phoneNumber;
        private string _operator;

        public int PhoneNumber { get; set; }
        public string Operator { get; set; }
        public Telephone()
        {

        }
        public Telephone(int phoneNumber, string oper)
        {
            this._phoneNumber = phoneNumber;
            this._operator = oper;
        }


        public override string ToString()
        {
            return string.Format("{0}/{1}", _phoneNumber, _operator);
        }


    }
}
