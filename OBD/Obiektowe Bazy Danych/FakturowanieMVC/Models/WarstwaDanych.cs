﻿using Db4objects.Db4o;
using Db4objects.Db4o.Config;
using Sharpen.IO;

namespace FakturowanieMVC.Models
{
    class WarstwaDanych
    {
        private IObjectContainer db;
        private IObjectSet result;
        readonly IEmbeddedConfiguration configuracja = Db4oEmbedded.NewConfiguration();
        private string path = "E:\\Faktury";
        public void PolaczenieZBazaDanych()
        {
            configuracja.Common.ObjectClass(typeof(Towar)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Towar)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Faktura)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Faktura)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(Kontrahent)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(Kontrahent)).CascadeOnUpdate(true);
            configuracja.Common.ObjectClass(typeof(PozycjaFaktury)).CascadeOnDelete(true);
            configuracja.Common.ObjectClass(typeof(PozycjaFaktury)).CascadeOnUpdate(true);

            db = Db4oEmbedded.OpenFile(configuracja, path);
        }

        public void ZamknijPolaczenie()
        {
            db.Close();
        }

        public int CzyIstnieje(object obiektDoSprawdzenia)
        {
            return db.QueryByExample(obiektDoSprawdzenia).Count;
        }

        public void DodajNowyObiekt(object obiektDoDodania)
        {
            db.Store(obiektDoDodania);
        }

        public IObjectSet WyszukajObiekt(object obiektDoWyszukania)
        {
            result = db.QueryByExample(obiektDoWyszukania);
            return result;
        }

        public void SkasujIstniejacyObiekt(object obiektDoSkasowania)
        {
            db.Delete(obiektDoSkasowania);
        }

        public void SkasujWszystkieObiekty()
        {
            var q = db.QueryByExample(typeof(Towar));
            foreach (var item in q)
            {
                db.Delete(item);
            }
        }
    }
}
