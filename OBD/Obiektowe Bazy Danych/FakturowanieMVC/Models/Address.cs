﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturowanieMVC.Models
{
    public class Address
    {
        // te własciowisci nie moga byc puste!
        public string Ulica { get; set; }
        public string Miasto { get; set; }
        public string KodPocztowy { get; set; }
    }
}