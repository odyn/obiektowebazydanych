﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturowanieMVC.Models
{
    public class Towar
    {
        public string Nazwa { get; set; }
        public enum StawkaVAT
        {
            DwadziesciaTrzy = 23,
            Osiem = 8,
            Zero = 0,
        }

        public double CenaBazowa { get; set; }
    }
}