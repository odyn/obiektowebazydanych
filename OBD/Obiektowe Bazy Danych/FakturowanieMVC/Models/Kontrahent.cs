﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FakturowanieMVC.Models
{
    public class Kontrahent
    {
        public string Nazwa { get; set; }
        public long? NIP { get; set; } // nie może być puste
        public Address AdresKontrahenta { get; set; } // referencja do obiektu
    }
}