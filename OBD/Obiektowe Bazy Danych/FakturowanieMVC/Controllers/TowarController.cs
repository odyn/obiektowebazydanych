﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakturowanieMVC.Models;

namespace FakturowanieMVC.Controllers
{
    public class TowarController : Controller
    {
        //
        // GET: /Towar/
        public ActionResult Index()
        {
            return View(bindTowar());
        }

        public List<Towar> bindTowar()
        {
            List<Towar> listaTowarow = new List<Towar>();

            foreach (var tow in listaTowarow)
            {
                listaTowarow.Add(new Towar());;
            }

            return listaTowarow;
        }

        //
        // GET: /Towar/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Towar/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Towar/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Towar/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Towar/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Towar/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Towar/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
