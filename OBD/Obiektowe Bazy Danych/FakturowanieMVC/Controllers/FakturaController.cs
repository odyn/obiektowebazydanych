﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakturowanieMVC.Models;

namespace FakturowanieMVC.Controllers
{
    public class FakturaController : Controller
    {
        //
        // GET: /Faktura/
        public ActionResult Index()
        {
            return View(bindFaktures());
        }

        public List<Faktura> bindFaktures()
        {
            List<Faktura> listaFaktur = new List<Faktura>();

            foreach (var fak in listaFaktur)
            {
               listaFaktur.Add(new Faktura());
            }

            return listaFaktur;
        }

        //
        // GET: /Faktura/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Faktura/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Faktura/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Faktura/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Faktura/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Faktura/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Faktura/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
