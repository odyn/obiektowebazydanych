﻿using System.Web.Mvc;
using FakturowanieMVC.Models;

namespace FakturowanieMVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly WarstwaDanych _db = new WarstwaDanych();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DodajKontrahenta(string nazwa, long? nip)
        {
            _db.PolaczenieZBazaDanych();
            var nowyKontrahent = new Kontrahent()
            {
                Nazwa = nazwa.ToString(),
                NIP = long.Parse(nip.ToString())
            };

            _db.DodajNowyObiekt(nowyKontrahent);
            _db.ZamknijPolaczenie();
            return View();
        }

        public ActionResult DodajAdres(string ulica, string miasto, string kod)
        {
            _db.PolaczenieZBazaDanych();
            var nowyadres = new Address()
            {
                Ulica = ulica,
                Miasto = miasto,
                KodPocztowy = kod
            };

            _db.DodajNowyObiekt(nowyadres);
            _db.ZamknijPolaczenie();
            return View();
        }

        public ActionResult DodajTowar()
        {
            return View();
        }

    }
}