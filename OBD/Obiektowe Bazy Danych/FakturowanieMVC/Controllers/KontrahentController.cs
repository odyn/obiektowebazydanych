﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FakturowanieMVC.Models;

namespace FakturowanieMVC.Controllers
{
    public class KontrahentController : Controller
    {
        //
        // GET: /Kontrahent/
        public ActionResult Index()
        {
            return View(bindKontrahents());
        }

        public List<Kontrahent> bindKontrahents()
        {
            List<Kontrahent> listaKontrahentow = new List<Kontrahent>();

            foreach (var kont in listaKontrahentow)
            {
                listaKontrahentow.Add(new Kontrahent());
            }

            return listaKontrahentow;
        }

        //
        // GET: /Kontrahent/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Kontrahent/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Kontrahent/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Kontrahent/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Kontrahent/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Kontrahent/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Kontrahent/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
