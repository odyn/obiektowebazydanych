﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Fakturowanie.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Adresy()
        {
            return View();
        }

        public ActionResult AdresyCreate()
        {
            return View();
        }

        public ActionResult Towary()
        {
            return View();
        }

        public ActionResult Faktury()
        {
            return View();
        }

        public ActionResult Kontrahenci()
        {
            return View();
        }

        
    }
}