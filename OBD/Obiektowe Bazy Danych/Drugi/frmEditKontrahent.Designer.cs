﻿namespace Drugi
{
    partial class frmEditKontrahent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxMiastoKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxKodPocztowyKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxUlicaKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxNIPKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxNazwaKontrahenta = new System.Windows.Forms.TextBox();
            this.btnSaveEditedChangesKontrahent = new System.Windows.Forms.Button();
            this.btnAnulujEdytowanieKontrahenta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 212);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Miasto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Kod pocztowy";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Ulica";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "NIP kontrahenta";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Nazwa kontrahenta";
            // 
            // tbxMiastoKontrahenta
            // 
            this.tbxMiastoKontrahenta.Location = new System.Drawing.Point(12, 228);
            this.tbxMiastoKontrahenta.Name = "tbxMiastoKontrahenta";
            this.tbxMiastoKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxMiastoKontrahenta.TabIndex = 15;
            // 
            // tbxKodPocztowyKontrahenta
            // 
            this.tbxKodPocztowyKontrahenta.Location = new System.Drawing.Point(12, 180);
            this.tbxKodPocztowyKontrahenta.Name = "tbxKodPocztowyKontrahenta";
            this.tbxKodPocztowyKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxKodPocztowyKontrahenta.TabIndex = 14;
            // 
            // tbxUlicaKontrahenta
            // 
            this.tbxUlicaKontrahenta.Location = new System.Drawing.Point(12, 133);
            this.tbxUlicaKontrahenta.Name = "tbxUlicaKontrahenta";
            this.tbxUlicaKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxUlicaKontrahenta.TabIndex = 13;
            // 
            // tbxNIPKontrahenta
            // 
            this.tbxNIPKontrahenta.Location = new System.Drawing.Point(12, 85);
            this.tbxNIPKontrahenta.Name = "tbxNIPKontrahenta";
            this.tbxNIPKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxNIPKontrahenta.TabIndex = 12;
            // 
            // tbxNazwaKontrahenta
            // 
            this.tbxNazwaKontrahenta.Location = new System.Drawing.Point(12, 37);
            this.tbxNazwaKontrahenta.Name = "tbxNazwaKontrahenta";
            this.tbxNazwaKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxNazwaKontrahenta.TabIndex = 11;
            // 
            // btnSaveEditedChangesKontrahent
            // 
            this.btnSaveEditedChangesKontrahent.Location = new System.Drawing.Point(12, 264);
            this.btnSaveEditedChangesKontrahent.Name = "btnSaveEditedChangesKontrahent";
            this.btnSaveEditedChangesKontrahent.Size = new System.Drawing.Size(151, 23);
            this.btnSaveEditedChangesKontrahent.TabIndex = 21;
            this.btnSaveEditedChangesKontrahent.Text = "Zapisz zmiany";
            this.btnSaveEditedChangesKontrahent.UseVisualStyleBackColor = true;
            // 
            // btnAnulujEdytowanieKontrahenta
            // 
            this.btnAnulujEdytowanieKontrahenta.Location = new System.Drawing.Point(219, 264);
            this.btnAnulujEdytowanieKontrahenta.Name = "btnAnulujEdytowanieKontrahenta";
            this.btnAnulujEdytowanieKontrahenta.Size = new System.Drawing.Size(135, 23);
            this.btnAnulujEdytowanieKontrahenta.TabIndex = 22;
            this.btnAnulujEdytowanieKontrahenta.Text = "Anuluj";
            this.btnAnulujEdytowanieKontrahenta.UseVisualStyleBackColor = true;
            this.btnAnulujEdytowanieKontrahenta.Click += new System.EventHandler(this.btnAnulujEdytowanieKontrahenta_Click);
            // 
            // frmEditKontrahent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(378, 308);
            this.Controls.Add(this.btnAnulujEdytowanieKontrahenta);
            this.Controls.Add(this.btnSaveEditedChangesKontrahent);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxMiastoKontrahenta);
            this.Controls.Add(this.tbxKodPocztowyKontrahenta);
            this.Controls.Add(this.tbxUlicaKontrahenta);
            this.Controls.Add(this.tbxNIPKontrahenta);
            this.Controls.Add(this.tbxNazwaKontrahenta);
            this.Name = "frmEditKontrahent";
            this.Text = "Edytuj istniejącego kontrahenta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxMiastoKontrahenta;
        private System.Windows.Forms.TextBox tbxKodPocztowyKontrahenta;
        private System.Windows.Forms.TextBox tbxUlicaKontrahenta;
        private System.Windows.Forms.TextBox tbxNIPKontrahenta;
        private System.Windows.Forms.TextBox tbxNazwaKontrahenta;
        private System.Windows.Forms.Button btnSaveEditedChangesKontrahent;
        private System.Windows.Forms.Button btnAnulujEdytowanieKontrahenta;
    }
}