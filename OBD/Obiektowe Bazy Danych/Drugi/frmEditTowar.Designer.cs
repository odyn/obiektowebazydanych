﻿namespace Drugi
{
    partial class frmEditTowar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnulujEdycjeTowaru = new System.Windows.Forms.Button();
            this.btnZapiszZmianyTowar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxCenaBazowaNowegoTowaru = new System.Windows.Forms.TextBox();
            this.comboStawkaVAT = new System.Windows.Forms.ComboBox();
            this.tbxNazwaNowegoTowaru = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAnulujEdycjeTowaru
            // 
            this.btnAnulujEdycjeTowaru.Location = new System.Drawing.Point(221, 167);
            this.btnAnulujEdycjeTowaru.Name = "btnAnulujEdycjeTowaru";
            this.btnAnulujEdycjeTowaru.Size = new System.Drawing.Size(179, 23);
            this.btnAnulujEdycjeTowaru.TabIndex = 15;
            this.btnAnulujEdycjeTowaru.Text = "Anuluj";
            this.btnAnulujEdycjeTowaru.UseVisualStyleBackColor = true;
            this.btnAnulujEdycjeTowaru.Click += new System.EventHandler(this.btnAnulujEdycjeTowaru_Click);
            // 
            // btnZapiszZmianyTowar
            // 
            this.btnZapiszZmianyTowar.Location = new System.Drawing.Point(22, 167);
            this.btnZapiszZmianyTowar.Name = "btnZapiszZmianyTowar";
            this.btnZapiszZmianyTowar.Size = new System.Drawing.Size(193, 23);
            this.btnZapiszZmianyTowar.TabIndex = 14;
            this.btnZapiszZmianyTowar.Text = "Zapisz zmiany";
            this.btnZapiszZmianyTowar.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Cena bazowa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Stawka VAT";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nazwa towaru";
            // 
            // tbxCenaBazowaNowegoTowaru
            // 
            this.tbxCenaBazowaNowegoTowaru.Location = new System.Drawing.Point(22, 137);
            this.tbxCenaBazowaNowegoTowaru.Name = "tbxCenaBazowaNowegoTowaru";
            this.tbxCenaBazowaNowegoTowaru.Size = new System.Drawing.Size(378, 20);
            this.tbxCenaBazowaNowegoTowaru.TabIndex = 10;
            // 
            // comboStawkaVAT
            // 
            this.comboStawkaVAT.FormattingEnabled = true;
            this.comboStawkaVAT.Items.AddRange(new object[] {
            "23%",
            "8%",
            "0%"});
            this.comboStawkaVAT.Location = new System.Drawing.Point(23, 86);
            this.comboStawkaVAT.Name = "comboStawkaVAT";
            this.comboStawkaVAT.Size = new System.Drawing.Size(377, 21);
            this.comboStawkaVAT.TabIndex = 9;
            // 
            // tbxNazwaNowegoTowaru
            // 
            this.tbxNazwaNowegoTowaru.Location = new System.Drawing.Point(22, 39);
            this.tbxNazwaNowegoTowaru.Name = "tbxNazwaNowegoTowaru";
            this.tbxNazwaNowegoTowaru.Size = new System.Drawing.Size(378, 20);
            this.tbxNazwaNowegoTowaru.TabIndex = 8;
            // 
            // frmEditTowar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 205);
            this.Controls.Add(this.btnAnulujEdycjeTowaru);
            this.Controls.Add(this.btnZapiszZmianyTowar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxCenaBazowaNowegoTowaru);
            this.Controls.Add(this.comboStawkaVAT);
            this.Controls.Add(this.tbxNazwaNowegoTowaru);
            this.Name = "frmEditTowar";
            this.Text = "Edytuj istniejący towar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAnulujEdycjeTowaru;
        private System.Windows.Forms.Button btnZapiszZmianyTowar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxCenaBazowaNowegoTowaru;
        private System.Windows.Forms.ComboBox comboStawkaVAT;
        private System.Windows.Forms.TextBox tbxNazwaNowegoTowaru;
    }
}