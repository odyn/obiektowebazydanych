﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drugi
{
    public class PozycjaFaktury
    {
        public Towar towar { get; set; } // referencja do obiektu towar
        public int Ilosc { get; set; }
        public double Cena { get; set; } // cena zaokraglona w gore do groszy

    }
}
