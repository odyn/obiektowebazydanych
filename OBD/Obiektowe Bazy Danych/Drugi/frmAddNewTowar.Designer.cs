﻿namespace Drugi
{
    partial class frmAddNewTowar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxNazwaNowegoTowaru = new System.Windows.Forms.TextBox();
            this.comboStawkaVAT = new System.Windows.Forms.ComboBox();
            this.tbxCenaBazowaNowegoTowaru = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDodajNowyTowar = new System.Windows.Forms.Button();
            this.btnAnulujDodawanieNowegoTowaru = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxNazwaNowegoTowaru
            // 
            this.tbxNazwaNowegoTowaru.Location = new System.Drawing.Point(12, 33);
            this.tbxNazwaNowegoTowaru.Name = "tbxNazwaNowegoTowaru";
            this.tbxNazwaNowegoTowaru.Size = new System.Drawing.Size(378, 20);
            this.tbxNazwaNowegoTowaru.TabIndex = 0;
            // 
            // comboStawkaVAT
            // 
            this.comboStawkaVAT.FormattingEnabled = true;
            this.comboStawkaVAT.Items.AddRange(new object[] {
            "23%",
            "8%",
            "0%"});
            this.comboStawkaVAT.Location = new System.Drawing.Point(13, 80);
            this.comboStawkaVAT.Name = "comboStawkaVAT";
            this.comboStawkaVAT.Size = new System.Drawing.Size(377, 21);
            this.comboStawkaVAT.TabIndex = 1;
            // 
            // tbxCenaBazowaNowegoTowaru
            // 
            this.tbxCenaBazowaNowegoTowaru.Location = new System.Drawing.Point(12, 131);
            this.tbxCenaBazowaNowegoTowaru.Name = "tbxCenaBazowaNowegoTowaru";
            this.tbxCenaBazowaNowegoTowaru.Size = new System.Drawing.Size(378, 20);
            this.tbxCenaBazowaNowegoTowaru.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nazwa towaru";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Stawka VAT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cena bazowa";
            // 
            // btnDodajNowyTowar
            // 
            this.btnDodajNowyTowar.Location = new System.Drawing.Point(12, 161);
            this.btnDodajNowyTowar.Name = "btnDodajNowyTowar";
            this.btnDodajNowyTowar.Size = new System.Drawing.Size(193, 23);
            this.btnDodajNowyTowar.TabIndex = 6;
            this.btnDodajNowyTowar.Text = "Dodaj nowy towar";
            this.btnDodajNowyTowar.UseVisualStyleBackColor = true;
            this.btnDodajNowyTowar.Click += new System.EventHandler(this.btnDodajNowyTowar_Click);
            // 
            // btnAnulujDodawanieNowegoTowaru
            // 
            this.btnAnulujDodawanieNowegoTowaru.Location = new System.Drawing.Point(211, 161);
            this.btnAnulujDodawanieNowegoTowaru.Name = "btnAnulujDodawanieNowegoTowaru";
            this.btnAnulujDodawanieNowegoTowaru.Size = new System.Drawing.Size(179, 23);
            this.btnAnulujDodawanieNowegoTowaru.TabIndex = 7;
            this.btnAnulujDodawanieNowegoTowaru.Text = "Anuluj";
            this.btnAnulujDodawanieNowegoTowaru.UseVisualStyleBackColor = true;
            this.btnAnulujDodawanieNowegoTowaru.Click += new System.EventHandler(this.btnAnulujDodawanieNowegoTowaru_Click);
            // 
            // frmAddNewTowar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 196);
            this.Controls.Add(this.btnAnulujDodawanieNowegoTowaru);
            this.Controls.Add(this.btnDodajNowyTowar);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxCenaBazowaNowegoTowaru);
            this.Controls.Add(this.comboStawkaVAT);
            this.Controls.Add(this.tbxNazwaNowegoTowaru);
            this.Name = "frmAddNewTowar";
            this.Text = "Dodaj nowy towar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxNazwaNowegoTowaru;
        private System.Windows.Forms.ComboBox comboStawkaVAT;
        private System.Windows.Forms.TextBox tbxCenaBazowaNowegoTowaru;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDodajNowyTowar;
        private System.Windows.Forms.Button btnAnulujDodawanieNowegoTowaru;
    }
}