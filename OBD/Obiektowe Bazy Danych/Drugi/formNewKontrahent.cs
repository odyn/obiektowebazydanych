﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Drugi
{
    public partial class formNewKontrahent : Form
    {
        readonly WarstwaDanych _db = new WarstwaDanych();
        public formNewKontrahent()
        {
            InitializeComponent();
        }

        private void btnCancelAddingNewKontrahent_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddNewKontrahent_Click(object sender, EventArgs e)
        {
            DodajNowegoKontrahenta();
        }

        public void DodajNowegoKontrahenta()
        {
            var newKontrahent = new Kontrahent()
            {
                Nazwa = tbxNazwaKontrahenta.Text,
                NIP = long.Parse(tbxNIPKontrahenta.Text)
            };

            var newAddress = new Adres()
            {
                Ulica = tbxUlicaKontrahenta.Text,
                KodPocztowy = tbxKodPocztowyKontrahenta.Text,
                Miasto = tbxMiastoKontrahenta.Text
            };

            _db.DodajNowyObiekt(newKontrahent);
        }
    }
}
