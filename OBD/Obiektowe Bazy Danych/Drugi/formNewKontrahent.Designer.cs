﻿namespace Drugi
{
    partial class formNewKontrahent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxNazwaKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxNIPKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxUlicaKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxKodPocztowyKontrahenta = new System.Windows.Forms.TextBox();
            this.tbxMiastoKontrahenta = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnAddNewKontrahent = new System.Windows.Forms.Button();
            this.btnCancelAddingNewKontrahent = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxNazwaKontrahenta
            // 
            this.tbxNazwaKontrahenta.Location = new System.Drawing.Point(13, 29);
            this.tbxNazwaKontrahenta.Name = "tbxNazwaKontrahenta";
            this.tbxNazwaKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxNazwaKontrahenta.TabIndex = 0;
            // 
            // tbxNIPKontrahenta
            // 
            this.tbxNIPKontrahenta.Location = new System.Drawing.Point(13, 77);
            this.tbxNIPKontrahenta.Name = "tbxNIPKontrahenta";
            this.tbxNIPKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxNIPKontrahenta.TabIndex = 1;
            // 
            // tbxUlicaKontrahenta
            // 
            this.tbxUlicaKontrahenta.Location = new System.Drawing.Point(13, 125);
            this.tbxUlicaKontrahenta.Name = "tbxUlicaKontrahenta";
            this.tbxUlicaKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxUlicaKontrahenta.TabIndex = 2;
            // 
            // tbxKodPocztowyKontrahenta
            // 
            this.tbxKodPocztowyKontrahenta.Location = new System.Drawing.Point(13, 172);
            this.tbxKodPocztowyKontrahenta.Name = "tbxKodPocztowyKontrahenta";
            this.tbxKodPocztowyKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxKodPocztowyKontrahenta.TabIndex = 3;
            // 
            // tbxMiastoKontrahenta
            // 
            this.tbxMiastoKontrahenta.Location = new System.Drawing.Point(13, 220);
            this.tbxMiastoKontrahenta.Name = "tbxMiastoKontrahenta";
            this.tbxMiastoKontrahenta.Size = new System.Drawing.Size(342, 20);
            this.tbxMiastoKontrahenta.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nazwa kontrahenta";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "NIP kontrahenta";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ulica";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Kod pocztowy";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Miasto";
            // 
            // btnAddNewKontrahent
            // 
            this.btnAddNewKontrahent.Location = new System.Drawing.Point(13, 257);
            this.btnAddNewKontrahent.Name = "btnAddNewKontrahent";
            this.btnAddNewKontrahent.Size = new System.Drawing.Size(145, 23);
            this.btnAddNewKontrahent.TabIndex = 11;
            this.btnAddNewKontrahent.Text = "Dodaj kontrahenta";
            this.btnAddNewKontrahent.UseVisualStyleBackColor = true;
            this.btnAddNewKontrahent.Click += new System.EventHandler(this.btnAddNewKontrahent_Click);
            // 
            // btnCancelAddingNewKontrahent
            // 
            this.btnCancelAddingNewKontrahent.Location = new System.Drawing.Point(213, 257);
            this.btnCancelAddingNewKontrahent.Name = "btnCancelAddingNewKontrahent";
            this.btnCancelAddingNewKontrahent.Size = new System.Drawing.Size(142, 23);
            this.btnCancelAddingNewKontrahent.TabIndex = 12;
            this.btnCancelAddingNewKontrahent.Text = "Anuluj";
            this.btnCancelAddingNewKontrahent.UseVisualStyleBackColor = true;
            this.btnCancelAddingNewKontrahent.Click += new System.EventHandler(this.btnCancelAddingNewKontrahent_Click);
            // 
            // formNewKontrahent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 292);
            this.Controls.Add(this.btnCancelAddingNewKontrahent);
            this.Controls.Add(this.btnAddNewKontrahent);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxMiastoKontrahenta);
            this.Controls.Add(this.tbxKodPocztowyKontrahenta);
            this.Controls.Add(this.tbxUlicaKontrahenta);
            this.Controls.Add(this.tbxNIPKontrahenta);
            this.Controls.Add(this.tbxNazwaKontrahenta);
            this.Name = "formNewKontrahent";
            this.Text = "Dodaj nowego kontrahenta";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxNazwaKontrahenta;
        private System.Windows.Forms.TextBox tbxNIPKontrahenta;
        private System.Windows.Forms.TextBox tbxUlicaKontrahenta;
        private System.Windows.Forms.TextBox tbxKodPocztowyKontrahenta;
        private System.Windows.Forms.TextBox tbxMiastoKontrahenta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnAddNewKontrahent;
        private System.Windows.Forms.Button btnCancelAddingNewKontrahent;
    }
}