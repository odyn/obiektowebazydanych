﻿namespace Drugi
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.dodajToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajKotrahentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajTowarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujKontrahentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujTowarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wystawToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.wystawFakturęToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zamknijToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tbxWyswietl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxRecordsCountInDB = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajToolStripMenuItem,
            this.edytujToolStripMenuItem,
            this.wystawToolStripMenuItem,
            this.zamknijToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // dodajToolStripMenuItem
            // 
            this.dodajToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajKotrahentaToolStripMenuItem,
            this.dodajTowarToolStripMenuItem});
            this.dodajToolStripMenuItem.Name = "dodajToolStripMenuItem";
            this.dodajToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.dodajToolStripMenuItem.Text = "Dodaj";
            // 
            // dodajKotrahentaToolStripMenuItem
            // 
            this.dodajKotrahentaToolStripMenuItem.Name = "dodajKotrahentaToolStripMenuItem";
            this.dodajKotrahentaToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.dodajKotrahentaToolStripMenuItem.Text = "Dodaj kotrahenta";
            this.dodajKotrahentaToolStripMenuItem.Click += new System.EventHandler(this.dodajKotrahentaToolStripMenuItem_Click);
            // 
            // dodajTowarToolStripMenuItem
            // 
            this.dodajTowarToolStripMenuItem.Name = "dodajTowarToolStripMenuItem";
            this.dodajTowarToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.dodajTowarToolStripMenuItem.Text = "Dodaj towar";
            this.dodajTowarToolStripMenuItem.Click += new System.EventHandler(this.dodajTowarToolStripMenuItem_Click);
            // 
            // edytujToolStripMenuItem
            // 
            this.edytujToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.edytujKontrahentaToolStripMenuItem,
            this.edytujTowarToolStripMenuItem});
            this.edytujToolStripMenuItem.Name = "edytujToolStripMenuItem";
            this.edytujToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.edytujToolStripMenuItem.Text = "Edytuj";
            // 
            // edytujKontrahentaToolStripMenuItem
            // 
            this.edytujKontrahentaToolStripMenuItem.Name = "edytujKontrahentaToolStripMenuItem";
            this.edytujKontrahentaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.edytujKontrahentaToolStripMenuItem.Text = "Edytuj kontrahenta";
            this.edytujKontrahentaToolStripMenuItem.Click += new System.EventHandler(this.edytujKontrahentaToolStripMenuItem_Click);
            // 
            // edytujTowarToolStripMenuItem
            // 
            this.edytujTowarToolStripMenuItem.Name = "edytujTowarToolStripMenuItem";
            this.edytujTowarToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.edytujTowarToolStripMenuItem.Text = "Edytuj towar";
            this.edytujTowarToolStripMenuItem.Click += new System.EventHandler(this.edytujTowarToolStripMenuItem_Click);
            // 
            // wystawToolStripMenuItem
            // 
            this.wystawToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.wystawFakturęToolStripMenuItem});
            this.wystawToolStripMenuItem.Name = "wystawToolStripMenuItem";
            this.wystawToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.wystawToolStripMenuItem.Text = "Wystaw";
            // 
            // wystawFakturęToolStripMenuItem
            // 
            this.wystawFakturęToolStripMenuItem.Name = "wystawFakturęToolStripMenuItem";
            this.wystawFakturęToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.wystawFakturęToolStripMenuItem.Text = "Wystaw fakturę";
            // 
            // zamknijToolStripMenuItem
            // 
            this.zamknijToolStripMenuItem.Name = "zamknijToolStripMenuItem";
            this.zamknijToolStripMenuItem.Size = new System.Drawing.Size(62, 20);
            this.zamknijToolStripMenuItem.Text = "Zamknij";
            this.zamknijToolStripMenuItem.Click += new System.EventHandler(this.zamknijToolStripMenuItem_Click);
            // 
            // tbxWyswietl
            // 
            this.tbxWyswietl.Location = new System.Drawing.Point(0, 24);
            this.tbxWyswietl.Multiline = true;
            this.tbxWyswietl.Name = "tbxWyswietl";
            this.tbxWyswietl.Size = new System.Drawing.Size(784, 367);
            this.tbxWyswietl.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(604, 401);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Rekordów w bazie";
            // 
            // tbxRecordsCountInDB
            // 
            this.tbxRecordsCountInDB.Enabled = false;
            this.tbxRecordsCountInDB.Location = new System.Drawing.Point(707, 398);
            this.tbxRecordsCountInDB.Name = "tbxRecordsCountInDB";
            this.tbxRecordsCountInDB.Size = new System.Drawing.Size(71, 20);
            this.tbxRecordsCountInDB.TabIndex = 3;
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 423);
            this.Controls.Add(this.tbxRecordsCountInDB);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxWyswietl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Program do faktur";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem dodajToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajKotrahentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajTowarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujKontrahentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujTowarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wystawToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem wystawFakturęToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zamknijToolStripMenuItem;
        private System.Windows.Forms.TextBox tbxWyswietl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxRecordsCountInDB;
    }
}

