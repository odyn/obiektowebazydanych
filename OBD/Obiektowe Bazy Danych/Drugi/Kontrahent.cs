﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drugi
{
    public class Kontrahent
    {
        public string Nazwa { get; set; }
        public long NIP { get; set; } // nie może być puste
        public Adres AdresKontrahenta { get; set; } // referencja do obiektu
    }
}
