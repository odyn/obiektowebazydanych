﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace Drugi
{
    public partial class main : Form
    {
        readonly WarstwaDanych _db = new WarstwaDanych();
        public main()
        {
            InitializeComponent();
            _db.PolaczenieZBazaDanych();
            WyswietlWszystkieRekordy();
        }

        private void zamknijToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void dodajKotrahentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f1 = new formNewKontrahent();
            f1.Show();
        }

        private void dodajTowarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f2 = new frmAddNewTowar();
            f2.Show();
        }

        private void edytujKontrahentaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f3 = new frmEditKontrahent();
            f3.Show();
        }

        private void edytujTowarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form f4 = new frmEditTowar();
            f4.Show();
        }

        public void WyswietlWszystkieRekordy()
        {
            tbxWyswietl.Clear();
            var k = new Kontrahent();
            var all = _db.WyszukajObiekt(k);
            tbxRecordsCountInDB.Text = all.Count.ToString() + "student/ów";

            foreach (var kon in all.Cast<Kontrahent>())
            {
                tbxWyswietl.Text += string.Format("Nazwa: {0} NIP: {1}", kon.Nazwa, kon.NIP, Environment.NewLine);

                tbxWyswietl.Text += Environment.NewLine;
            }
        }
    }
}
