﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;
using FakturowanieZad2.Security;

namespace FakturowanieZad2
{
    public partial class CreateContractorWindow : Window
    {
        public List<TextBox> tbxList = new List<TextBox>();
        public CreateContractorWindow()
        {
            InitializeComponent();

            tbxList.Add(ContractorNameTextBox);
            tbxList.Add(NumberNipTextBox);
            tbxList.Add(StreeTextBox);
            tbxList.Add(PostalCodeTextBox);
            tbxList.Add(CityTextBox);
        }

        private void AddNewContractorButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            var validate = new ValidateControlls();
            var errors = new List<string>();

            errors.Add(validate.CzyPuste(tbxList)); // sprawdzamy czy pola sa puste

            errors.Add(validate.CzyNipZawiera10Cyfr(NumberNipTextBox.Text)); // sprawdzam czy NIP zawiera 10 cyfr i jest NIPem

            errors.Add(validate.CzyKodPocztowy(PostalCodeTextBox.Text)); // czy kod pocztowy ma odpowiedni format 00-000

            errors.Add(validate.CzyMiastoNieZawieraCyfr(CityTextBox.Text)); // czy miasto składa sie tylko z liter bez cyfr

            //errors.Add(validate.CzyDobraUlica(StreeTextBox.Text)); // czy ulica nie zawiera złych znakow

            errors.Add(validate.CzyNipJuzIstnieje(NumberNipTextBox.Text)); // czy wpisany NIP już istnieje

            errors.Where(n => n != null).ToList().ForEach(n => LabelError.Content += string.Format("{0}\n", n)); // zwracamy aktualne bledy walidacji

            if (errors.Count(n => n != null) > 0)
            {
                LabelError.Visibility = Visibility.Visible;
                errors = new List<string>();
                return;
            }

            // tworzenie obiektu nowego kontrachenta
            var contr = new Contractor()
            {
                Address = new Address(CityTextBox.Text, StreeTextBox.Text, PostalCodeTextBox.Text),
                ContractorName = ContractorNameTextBox.Text,
                NipNumber = NumberNipTextBox.Text
            };

            // dodawanie obiektu do bazy
            DataBaseConnection.db.Store(contr);
            DataBaseConnection.db.Commit();

            LabelError.Visibility = Visibility.Visible;
            LabelError.Foreground = Brushes.Green;
            LabelError.Content = "Dodano Kontrahenta pomyślnie!";

            // czyszczenie textboxów po dodaniu
            foreach (var cbx in tbxList)
            {
                cbx.Text = "";
            }

            //zamykanie formularza
            this.Close();

           
        }

        private void CancelAddingContractorButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
