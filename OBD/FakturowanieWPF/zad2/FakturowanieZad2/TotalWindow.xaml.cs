﻿using System.Globalization;
using System.Linq;
using System.Windows;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    public partial class TotalWindow : Window
    {
        private readonly Invoice invoice;
        public TotalWindow(Invoice inv)
        {
            InitializeComponent();
            invoice = inv;

            ShowTotalValues();
        }

        private void ShowTotalValues()
        {
            invoice.CreateTotality();

            Tb0.Text = invoice.Totality.First(n => n.Tax == "0%").Price.ToString(CultureInfo.InvariantCulture);
            Tb8.Text = invoice.Totality.First(n => n.Tax == "8%").Price.ToString(CultureInfo.InvariantCulture);
            Tb23.Text = invoice.Totality.First(n => n.Tax == "23%").Price.ToString(CultureInfo.InvariantCulture);
            Tbzw.Text = invoice.Totality.First(n => n.Tax == "zwolniony").Price.ToString(CultureInfo.InvariantCulture);
        }

        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
