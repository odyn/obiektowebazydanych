﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FakturowanieZad2.Models
{
    public class Invoice
    {
        public string DateOfIssue { get; set; }

        public int InvoiceNumber{ get; set; }

        public Contractor Reciever { get; set; }
        public List<InvoicePossition> Commoditions { get; set; }

        private string _tax;

        public string Tax // podatek
        {
            get { return CalculateTax(); }
            set { _tax = value; }
        }

        private string _amount;
        private double amountDouble = 0;

        public string Amount // kwota
        {
            get { return CalculateAmount(); }
            set { _amount = value; }
        }

        private string _grossAmount;
        public string GrossAmount // kwota brutto
        {
            get
            {
                NumberFormatInfo provider = new NumberFormatInfo();
                provider.NumberDecimalSeparator = ".";
                provider.NumberGroupSeparator = ",";
                var gross = (Convert.ToDouble(Amount,provider) + Convert.ToDouble(Tax,provider));
                // zwraca kwote + podatek
                return String.Format(provider,"{0:0.00}", gross);
            }
            set { _grossAmount = value; }
        }

        public List<Taxes> Totality { get; set; } // zestawienie

        // oblicza podatek wg zadanej stopy procentowej
        private string CalculateTax()
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberGroupSeparator = ",";

            double amountOfTax = 0;

            if (Totality == null)
                return "00.000";

            foreach (var zest in Totality)
            {
                if (zest.Tax == "8%")
                    amountOfTax += (Convert.ToDouble(zest.Price,provider) * 8) / 100;

                if (zest.Tax == "23%")
                    amountOfTax += (Convert.ToDouble(zest.Price,provider) * 23) / 100;
            }

            return String.Format(provider,"{0:0.00}",Convert.ToDouble(amountOfTax,provider));
        }

        // oblicza kwotę
        private string CalculateAmount()
        {
           

            // jesli brak towarów
            if (Commoditions == null) 
                return _amount = "00.000";

            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberGroupSeparator = ",";

            // oblicz kwote ( cenna netto * ilość )
            var amDouble = Commoditions.Sum(towar => Convert.ToDouble(towar.PriceNetto,provider) * towar.Quantity);

           

            var tmp =  String.Format(provider,"{0:0.00}",Convert.ToDouble(amDouble,provider));
            return tmp;
        }

        // generuje zestawienie
        public void CreateTotality()
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberGroupSeparator = ",";

            if (Totality != null)
                Totality.Clear();

            Totality = new List<Taxes>
            {
                new Taxes("23%", "00.000"),
                new Taxes("8%", "00.000"),
                new Taxes("0%", "00.000"),
                new Taxes("zwolniony", "00.000")
            };

            if (Commoditions == null)
                return;



            foreach (var commodity in Commoditions.Where(commodity => commodity.Commodity.Amount == "23%"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto, provider);
                Totality.First(n => n.Tax == "23%").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }

            amountDouble = 0;
            foreach (var commodity in Commoditions.Where(commodity => commodity.Commodity.Amount == "8%"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto, provider);
                Totality.First(n => n.Tax == "8%").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }

            amountDouble = 0;
            foreach (var commodity in Commoditions.Where(commodity => commodity.Commodity.Amount == "0%"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto, provider);
                Totality.First(n => n.Tax == "0%").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }

            amountDouble = 0;
            foreach (var commodity in Commoditions.Where(towar => towar.Commodity.Amount == "zwolniony"))
            {
                amountDouble += Convert.ToDouble(commodity.PriceNetto, provider);
                Totality.First(n => n.Tax == "zwolniony").Price = amountDouble.ToString(CultureInfo.InvariantCulture);
            }
        }
    }
}
