﻿namespace FakturowanieZad2.Models
{
    public class Taxes
    {
        public Taxes(string tax, string price)
        {
            Tax = tax;
            Price = price;
        }
        public string Tax { get; set; }
        public string Price { get; set; }
    }
}
