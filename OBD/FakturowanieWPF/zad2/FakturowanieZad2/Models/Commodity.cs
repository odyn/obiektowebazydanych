﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakturowanieZad2.Models
{
    public class Commodity
    {
        public string Amount { get; set; }
        public string CommodityName { get; set; }
        private string _nettoPrice;

        private NumberFormatInfo provider = new NumberFormatInfo
        {
            NumberDecimalSeparator = ".",
            NumberGroupSeparator = ","
        };
        
        public string NettoPrice
        {
            get { return _nettoPrice; }
            set { _nettoPrice = value; }
        }
    }
}
