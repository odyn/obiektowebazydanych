﻿using System;

namespace FakturowanieZad2.Models
{
    public class InvoicePossition
    {
        public Commodity Commodity { get; set; }
        public int Quantity { get; set; }

        private string _priceNetto;

        public string PriceNetto
        {
            get { return _priceNetto; }
            set { _priceNetto = value; }
        }
    }
}
