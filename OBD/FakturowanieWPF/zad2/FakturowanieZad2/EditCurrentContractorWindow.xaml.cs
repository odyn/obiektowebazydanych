﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;
using FakturowanieZad2.Security;

namespace FakturowanieZad2
{
    public partial class EditCurrentContractorWindow : Window
    {
        public List<TextBox> tbxList= new List<TextBox>();
        private readonly Contractor contractorToEdit;

        public EditCurrentContractorWindow(Contractor contractor)
        {
            InitializeComponent();
            Title = "Edycja dla " + contractor.ContractorName;

            contractorToEdit = contractor;

            tbxList.Add(ContractorNameTextBox);
            tbxList.Add(NipNumberTextBox);
            tbxList.Add(StreetTextBox);
            tbxList.Add(PostalCodeTextBox);
            tbxList.Add(CityTextBox);

            LoadExsistingData();
        }

        public void LoadExsistingData()
        {
            ContractorNameTextBox.Text = contractorToEdit.ContractorName;
            NipNumberTextBox.Text = contractorToEdit.NipNumber;
            StreetTextBox.Text = contractorToEdit.Address.Street;
            PostalCodeTextBox.Text = contractorToEdit.Address.PostalCode;
            CityTextBox.Text = contractorToEdit.Address.City;
        }

        private void CancelEditionButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveChangesButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            var validate = new ValidateControlls();
            var errors = new List<string>();

            errors.Add(validate.CzyPuste(tbxList)); // sprawdzamy czy pola sa puste

            errors.Add(validate.CzyNipZawiera10Cyfr(NipNumberTextBox.Text)); // sprawdzam czy NIP zawiera 10 cyfr i jest NIPem

            errors.Add(validate.CzyKodPocztowy(PostalCodeTextBox.Text)); // czy kod pocztowy ma odpowiedni format 00-000

            errors.Add(validate.CzyMiastoNieZawieraCyfr(CityTextBox.Text)); // czy miasto składa sie tylko z liter bez cyfr

            //errors.Add(validate.CzyDobraUlica(StreetTextBox.Text)); // czy ulica nie zawiera nie dozwolonych znaków

            errors.Add(validate.CzyNipJuzIstnieje(NipNumberTextBox.Text)); // czy wpisany NIP już istnieje

            errors.Where(n => n != null).ToList().ForEach(n => LabelError.Content += string.Format("{0}\n", n)); // zwracamy aktualne bledy walidacji

            if (errors.Count(n => n != null) > 0)
            {
                LabelError.Visibility = Visibility.Visible;
                errors = new List<string>();
                return;
            }
           

            // wyciaganie kontrahenta z bazy
            var any = DataBaseConnection.db.Query<Contractor>().FirstOrDefault(n => n.NipNumber == NipNumberTextBox.Text);

           


            // przepisywanie nowych wartości do obiektu
            contractorToEdit.ContractorName = ContractorNameTextBox.Text;
            contractorToEdit.NipNumber = NipNumberTextBox.Text;
            contractorToEdit.Address.Street = StreetTextBox.Text;
            contractorToEdit.Address.PostalCode = PostalCodeTextBox.Text;
            contractorToEdit.Address.City = CityTextBox.Text;

            // zapisywanie nowego obiektu do bazy
            DataBaseConnection.db.Store(contractorToEdit);
            DataBaseConnection.db.Commit();

            this.Close();
        }
    }
}
