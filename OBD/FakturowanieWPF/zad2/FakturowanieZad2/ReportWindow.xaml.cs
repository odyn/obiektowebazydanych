﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using FakturowanieZad2.Models;


namespace FakturowanieZad2
{
    public partial class ReportWindow
    {
        public IList<Invoice> invoices;
        public List<Report> reports;

        public ReportWindow(IList<Invoice> invoice)
        {
            invoices = invoice;

            InitializeComponent();

            ReportDataGrid.ItemsSource = null;

            GenerateReport();
        }

        public void GenerateReport()
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberGroupSeparator = ",";

            reports = new List<Report>();
            foreach (var inv in invoices)
            {
                if (inv == null) continue;
                if (inv.Reciever == null) continue;
                if (inv.Reciever.Address == null) continue;
                if (inv.Reciever.Address.PostalCode == null) continue;
                if (inv.Reciever.Address.PostalCode.Length == 0) continue;

                if (reports.Where(n => n.Localisation == inv.Reciever.Address.PostalCode[0].ToString()).Count() == 0)
                {
                    reports.Add(new Report()
                    {
                        Localisation = inv.Reciever.Address.PostalCode[0].ToString(),
                        SumOfAmount = "00.000"
                    });
                }

                var ret = reports.Where(n => n.Localisation == inv.Reciever.Address.PostalCode[0].ToString());

                ret.First().SumOfAmount = Convert.ToString(Convert.ToDouble(ret.First().SumOfAmount,provider) + Convert.ToDouble(inv.GrossAmount,provider));
            }
            ReportDataGrid.ItemsSource = reports;

        }
    

        private void CancelButton_OnClickButton_OnClick(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
