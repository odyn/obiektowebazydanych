﻿using System.Collections.Generic;

namespace FakturowanieZad2.Services
{
    public static class Service
    {
        static Service()
        {
            GetTaxOptions = new[] { "0%", "8%", "23%", "zwolniony" };
        }
        // lista zawierająca dostępne podatki
        public static IList<string> GetTaxOptions { get; set; }
    }
}
