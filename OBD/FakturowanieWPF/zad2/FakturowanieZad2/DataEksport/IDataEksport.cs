﻿namespace FakturowanieZad2.DataEksport
{
    public interface IDataEksport
    {
        void SaveToTextFile();
        void SaveToPdfFile();
        void SaveToXmlFile();
    }
}
