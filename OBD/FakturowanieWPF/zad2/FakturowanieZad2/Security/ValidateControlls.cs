﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2.Security
{
    public class ValidateControlls
    {
        public string SprawdzCenne(string tbx)
        {
            if (Regex.IsMatch(tbx, @"^[0-9\.\,]+$") == false)
            {
                return "Pole cena netto musi zawierać tylko cyfry!";
            }

            return null;
        }

        public string CzyWartoscDodatnia(string tbxIlosc)
        {
            if (tbxIlosc.Contains("-"))
            {
                return tbxIlosc.Replace("-", "");
            }
            return tbxIlosc;
        }

        public string CzyZawieraCyfry(string tbx)
        {
            if (Regex.IsMatch(tbx, @"\d") == false)
            {
                return "Pole ilosc może zawierac tylko cyfry!";
            }

            return null;
        }

        public string CzyNipZawiera10Cyfr(string tbx)
        {
            if (Regex.IsMatch(tbx, @"^\d{10}$") == false)
            {
                return "Numer NIP musi zawierac 10 cyfr!";
            }

            return null;
        }

        public string CzyNipJuzIstnieje(string tbx)
        {
            if (DataBaseConnection.db.Query<Contractor>().Any(n => n.NipNumber == tbx))
            {
                var cont = DataBaseConnection.db.Query<Contractor>().First(n => n.NipNumber == tbx);
                return "Podany numer NIP juz istnieje!";
            }
            return null;
        }

        public string CzyMiastoNieZawieraCyfr(string tbx)
        {
            if (Regex.IsMatch(tbx, @"\d"))
            {
                return "Pole miasto nie może zawierać cyfr!";
            }
            return null;
        }

        public string CzyKodPocztowy(string tbx)
        {
            if (Regex.IsMatch(tbx, @"[0-9]{2}-[0-9]{3}") == false)
            {
                return "Zły format kodu pocztowego!";
            }
            return null;
        }

        public string CzyDobraUlica(string tbx)
        {
            if (Regex.IsMatch(tbx, @"^([0-9]+ )?[a-zA-Z ]+$") == false)
            {
                return "Zła ulica!";
            }
            return null;
        }

        public string ZwrocCenne(string tbx)
        {
            //if (tbx.Contains("."))
            //{
            //    return tbx.Replace('.', ',');
            //}

            return tbx;
        }

        public string CzyWybranoStawkeVat(ComboBox tbx)
        {
            if (tbx.SelectedValue == null)
            {
                return "Prosze wybrac stawke VAT!";
            }

            return null;
        }

        public string CzyPuste(List<TextBox> tbxList)
        {
            foreach (var tbx in tbxList.Where(textBox => textBox.Text == ""))
            {
                return "Wszystkie pola sa wymagane!";
            }
            return null;
        }


    }
}
