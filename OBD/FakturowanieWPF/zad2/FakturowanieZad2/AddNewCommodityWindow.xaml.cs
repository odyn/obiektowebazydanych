﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;
using FakturowanieZad2.Security;
using FakturowanieZad2.Services;


namespace FakturowanieZad2
{

    public partial class AddNewCommodityWindow : Window
    {
        public Invoice invoice;
        public List<TextBox> tbxList = new List<TextBox>();
        public AddNewCommodityWindow(Invoice invoice)
        {
            this.invoice = invoice;
            InitializeComponent();

            foreach (var serviceList in Service.GetTaxOptions)
            {
                CbSearchList.Items.Add(serviceList);
            }

            tbxList.Add(CommoditionNameTextBox);
            tbxList.Add(NettoPriceTextBox);
            tbxList.Add(QuantityTextBox);
        }

        private void CancelAddingCommodityButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddNewComodityToInvoiceButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = "";
            LabelError.Foreground = Brushes.Red;

            var validation = new ValidateControlls();
            var errors = new List<string>();

            errors.Add(validation.CzyPuste(tbxList)); // sprawdzamy czy pola sa puste

            errors.Add(validation.SprawdzCenne(NettoPriceTextBox.Text)); // czy cena zawiera tylko liczby
            NettoPriceTextBox.Text = validation.ZwrocCenne(NettoPriceTextBox.Text); // zamieniamy kropke na przecinek
            NettoPriceTextBox.Text = validation.CzyWartoscDodatnia(NettoPriceTextBox.Text); // zamieniamy kropke na przecinek

            errors.Add(validation.CzyZawieraCyfry(QuantityTextBox.Text)); // czy ilosc zawiera cyfry
            QuantityTextBox.Text = validation.CzyWartoscDodatnia(QuantityTextBox.Text); // czy ilosc jest dodatnia

            errors.Add(validation.CzyWybranoStawkeVat(CbSearchList)); // czy wybrano stawke VAT

            errors.Where(n => n != null).ToList().ForEach(n => LabelError.Content += string.Format("{0}\n",n)); // zwracamy aktualne bledy walidacji

            if (errors.Count(n => n != null)>0)
            {
                LabelError.Visibility = Visibility.Visible;
                errors = new List<string>();
                return;
            }

            // jeśli faktura jest pusta 
            if (invoice.Commoditions == null)
                invoice.Commoditions = new List<InvoicePossition>();

            // dodawanie nowych towarów
            invoice.Commoditions.Add(new InvoicePossition
            {
                PriceNetto = NettoPriceTextBox.Text,
                Quantity = Convert.ToInt32(QuantityTextBox.Text),
                Commodity = new Commodity
                {
                    NettoPrice = NettoPriceTextBox.Text,
                    CommodityName = CommoditionNameTextBox.Text,
                    Amount = CbSearchList.SelectedValue.ToString()
                }
            });

            DataBaseConnection.db.Store(invoice);
            DataBaseConnection.db.Commit();

            LabelError.Visibility = Visibility.Visible;
            LabelError.Foreground = Brushes.Green;
            LabelError.Content = "Dodano Towar pomyślnie!";

            foreach (var contr in tbxList)
            {
                contr.Text = "";
            }
            Close();
        }
    }
}
