﻿using System;
using System.Windows;
using System.Windows.Controls;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    /// <summary>
    /// Interaction logic for CommoditiesWindow.xaml
    /// </summary>
    public partial class CommoditiesWindow : Window
    {
        private readonly Invoice invoice;
        public CommoditiesWindow(Invoice inv)
        {
            this.invoice = inv;
            InitializeComponent();
            
            Title = "Odbiorca "+inv.Reciever.ContractorName + " - Faktura nr. " + inv.InvoiceNumber;

            ShowAllExsistingContrahents();
        }

        private void ShowAllExsistingContrahents()
        {
            DataGridViewCommodities.ItemsSource = null;
            DataGridViewCommodities.CommitEdit();

            DataGridViewCommodities.ItemsSource = invoice.Commoditions;
        }

        private void CommoditiesWindow_OnActivated(object sender, EventArgs e)
        {
            ShowAllExsistingContrahents();
        }

        private void AddNewCommodityButton_Click(object sender, RoutedEventArgs e)
        {
            new AddNewCommodityWindow(invoice).Show();
        }

        private void RemoveCommodity_Click(object sender, RoutedEventArgs e)
        {
            var positionToRemove = ((FrameworkElement)sender).DataContext as InvoicePossition;

            invoice.Commoditions.Remove(positionToRemove);

            DataBaseConnection.db.Store(invoice);
            DataBaseConnection.db.Commit();

            ShowAllExsistingContrahents();
        }

        private void EditCommodities_Click(object sender, RoutedEventArgs e)
        {
            var editoCommodity = ((FrameworkElement)sender).DataContext as InvoicePossition;
            new EditCommodityWindow(invoice, editoCommodity).Show();
        }

        private void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = e.Row.GetIndex() + 1;
        }

        private void CancelAddingNewCommodityButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
