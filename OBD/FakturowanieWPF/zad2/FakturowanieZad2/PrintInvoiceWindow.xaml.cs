﻿using System;
using System.Globalization;
using System.Windows;
using FakturowanieZad2.Models;

namespace FakturowanieZad2
{
    public partial class PrintInvoiceWindow : Window
    {
        private Invoice _invoice;
        public PrintInvoiceWindow(Contractor contractor)
        {
            InitializeComponent();
        }

        public PrintInvoiceWindow(Invoice invoice)
        {
            _invoice = invoice;
            InitializeComponent();

            PrintEverythink();
        }

        public void PrintEverythink()
        {
            NumberFormatInfo provider = new NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            provider.NumberGroupSeparator = ",";

            Lp.Height = double.NaN;
            Title = "Druk dla " +_invoice.Reciever.ContractorName;

            Faktura.Text = "Data wystawienia: " + _invoice.DateOfIssue + "\nNumer: " + _invoice.InvoiceNumber;
            Odbiorca.Text = "Kontrahent: " + _invoice.Reciever.ContractorName + "\nNIP: " + _invoice.Reciever.NipNumber + "\nUlica: " + _invoice.Reciever.Address.Street + "\nKodPocztowy: " + _invoice.Reciever.Address.PostalCode + "\nMiasto: " + _invoice.Reciever.Address.City + "\n\n";

            if (_invoice.Commoditions != null)
            {
                var i = 1;
                foreach (var position in _invoice.Commoditions)
                {
                    Lp.Text += i + ".\n";
                    Nazwa.Text += position.Commodity.CommodityName + "\n";
                    CenaJdn.Text += position.PriceNetto + "\n";
                    Jm.Text += "szt.\n";
                    Ilosc.Text += position.Quantity + "\n";
                    Vat.Text += position.Commodity.Amount + "\n";

                    var brussCommodityValue = Convert.ToDouble(position.Commodity.NettoPrice, provider) * Convert.ToDouble(position.Quantity, provider);
                    WartNetto.Text += String.Format(provider, "{0:0.00}", brussCommodityValue) + "\n";

                    double brussValue = 0;
                    if (position.Commodity.Amount == "0%")
                        brussValue = brussCommodityValue;
                    if (position.Commodity.Amount == "8%")
                        brussValue = brussCommodityValue * 108 / 100;
                    if (position.Commodity.Amount == "23%")
                        brussValue = brussCommodityValue * 123 / 100;
                    if (position.Commodity.Amount == "zwolniony")
                        brussValue = brussCommodityValue;

                    WartBrutto.Text += String.Format(provider, "{0:0.00}", brussValue) + "\n";
                    i++;
                }
            }

            ToPayTbx.Text = _invoice.GrossAmount + " zł";

            if (_invoice.Totality != null)
            {
                foreach (var tot in _invoice.Totality)
                {
                    double brussTotalityValue = 0;
                    if (tot.Tax == "0%")
                        brussTotalityValue = Convert.ToDouble(tot.Price, provider);
                    if (tot.Tax == "8%")
                        brussTotalityValue = Convert.ToDouble(tot.Price, provider) * 108 / 100;
                    if (tot.Tax == "23%")
                        brussTotalityValue = Convert.ToDouble(tot.Price, provider) * 123 / 100;
                    if (tot.Tax == "zwolniony")
                        brussTotalityValue = Convert.ToDouble(tot.Price, provider);

                    double vatValue = 0;
                    if (tot.Tax == "0%")
                        vatValue = 0;
                    if (tot.Tax == "8%")
                        vatValue = Convert.ToDouble(tot.Price, provider) * 8 / 100;
                    if (tot.Tax == "23%")
                        vatValue = Convert.ToDouble(tot.Price, provider) * 23 / 100;
                    if (tot.Tax == "zwolniony")
                        vatValue = 0;
                }
            }

            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SaveToFile_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
