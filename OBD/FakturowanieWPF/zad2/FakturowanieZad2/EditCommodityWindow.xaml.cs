﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using FakturowanieZad2.DataBase;
using FakturowanieZad2.Models;
using FakturowanieZad2.Security;


namespace FakturowanieZad2
{
    public partial class EditCommodityWindow : Window
    {
        public Invoice invoice;
        private readonly InvoicePossition editCommodition;

        public List<TextBox> createTextBoxes = new List<TextBox>();
        public EditCommodityWindow()
        {
            InitializeComponent();
        }

        public EditCommodityWindow(Invoice inv, InvoicePossition editCommodity)
        {
            invoice = inv;
            editCommodition = editCommodity;

            InitializeComponent();
            
            LoadInitValues();

            createTextBoxes.Add(CommodityNameTextBox);
            createTextBoxes.Add(PriceNettoTextBox);
            createTextBoxes.Add(QuantityTextBox);
        }
        
        private void LoadInitValues()
        {
            Title = editCommodition.Commodity.CommodityName + "-" + editCommodition.PriceNetto.ToString(CultureInfo.InvariantCulture) + "-" +
                    editCommodition.Quantity.ToString(CultureInfo.InvariantCulture) + "-" + editCommodition.Commodity.Amount;

            CommodityNameTextBox.Text = editCommodition.Commodity.CommodityName;
            PriceNettoTextBox.Text = editCommodition.PriceNetto.ToString(CultureInfo.InvariantCulture);
            QuantityTextBox.Text = editCommodition.Quantity.ToString(CultureInfo.InvariantCulture);

            CbSearchList.SelectedValue = editCommodition.Commodity.Amount.ToList();
        }


        private void CancelEditingButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveChangesButton_Click(object sender, RoutedEventArgs e)
        {
            LabelError.Content = string.Empty;
            LabelError.Foreground = Brushes.Red;

            var validation = new ValidateControlls();
            var errors = new List<string>();

            errors.Add(validation.CzyPuste(createTextBoxes)); // sprawdzamy czy pola sa puste

            errors.Add(validation.SprawdzCenne(PriceNettoTextBox.Text)); // czy cena zawiera tylko liczby
            PriceNettoTextBox.Text = validation.ZwrocCenne(PriceNettoTextBox.Text); // zamieniamy kropke na przecinek
            PriceNettoTextBox.Text = validation.CzyWartoscDodatnia(PriceNettoTextBox.Text); // zamieniamy kropke na przecinek

            errors.Add(validation.CzyZawieraCyfry(QuantityTextBox.Text)); // czy ilosc zawiera cyfry
            QuantityTextBox.Text = validation.CzyWartoscDodatnia(QuantityTextBox.Text); // czy ilosc jest dodatnia

            errors.Add(validation.CzyWybranoStawkeVat(CbSearchList)); // czy wybrano stawke VAT

            errors.Where(n => n != null).ToList().ForEach(n => LabelError.Content += string.Format("{0}\n", n)); // zwracamy aktualne bledy walidacji

            if (errors.Count(n => n != null) > 0)
            {
                LabelError.Visibility = Visibility.Visible;
                errors = new List<string>();
                return;
            }

            foreach (var position in invoice.Commoditions.Where(position => position == editCommodition))
            {
                position.Quantity = Convert.ToInt32(QuantityTextBox.Text);
                position.PriceNetto = PriceNettoTextBox.Text;
                position.Commodity = new Commodity { NettoPrice = PriceNettoTextBox.Text, CommodityName = CommodityNameTextBox.Text, Amount = CbSearchList.SelectedValue.ToString() };
            }

            DataBaseConnection.db.Store(invoice);
            //DataBaseConnection.db.Ext().Store(invoice,int.MaxValue);
            DataBaseConnection.db.Commit();

            var bu = DataBaseConnection.db.Query<Invoice>().ToList<Invoice>();

            LabelError.Visibility = Visibility.Visible;
            LabelError.Foreground = Brushes.Green;

            Close();
        }
    }
}
